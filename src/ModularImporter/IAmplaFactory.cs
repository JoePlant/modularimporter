﻿using AmplaData;

namespace ModularImporter
{
    public interface IAmplaFactory
    {
        IRepository<TModel> GetRepository<TModel>() where TModel : class, new();
    }
}