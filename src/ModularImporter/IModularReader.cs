﻿using System.Collections.Generic;
using ModularImporter.Models.Modular;

namespace ModularImporter
{
    public interface IModularReader
    {
        List<StatusEvent> ReadStatusEventsRecords();

        List<ExpRoot> ReadExpRootRecords();

        List<EqmtList> ReadEqmtListRecords();

        List<OperList> ReadOperListRecords();

        List<ReasonTable> ReadReasonTableRecords();

        List<Enums> ReadEnumRecords();

        List<Dumps> ReadDumpRecords();

        List<GradeList> ReadGradeRecords();
    }
}