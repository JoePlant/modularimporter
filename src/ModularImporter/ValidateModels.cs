﻿using System.Collections.Generic;
using System.Linq;
using AmplaData.AmplaData2008;
using AmplaData.AmplaRepository;
using ModularImporter.Models.Ampla;

namespace ModularImporter
{
    /// <summary>
    ///     Validates the models used
    /// </summary>
    public class ValidateModels
    {
        private readonly ICredentialsProvider credentials;

        public ValidateModels(ICredentialsProvider credentials)
        {
            this.credentials = credentials;
        }

        /// <summary>
        /// Validates this models and returns any messages.
        /// </summary>
        /// <returns></returns>
        public List<string> Validate()
        {
            using (DataWebServiceClient client = new DataWebServiceClient("NetTcpBinding_IDataWebService"))
            {
                List<string> messages = new List<string>();
                using (AmplaRepository<DowntimeModel> repository = new AmplaRepository<DowntimeModel>(client, credentials))
                {
                    var problems = repository.ValidateMapping(new DowntimeModel());
                    messages.AddRange(problems.Select(message => "Downtime: " + message));
                }

                using (AmplaRepository<DumpModel> repository = new AmplaRepository<DumpModel>(client, credentials))
                {
                    var problems = repository.ValidateMapping(new DumpModel());
                    messages.AddRange(problems.Select(message => "Dump: " + message));
                }

                using (AmplaRepository<GradeModel> repository = new AmplaRepository<GradeModel>(client, credentials))
                {
                    var problems = repository.ValidateMapping(new GradeModel());
                    messages.AddRange(problems.Select(message => "Grade: " + message));
                }

                using (AmplaRepository<LoadModel> repository = new AmplaRepository<LoadModel>(client, credentials))
                {
                    var problems = repository.ValidateMapping(new LoadModel());
                    messages.AddRange(problems.Select(message => "Load: " + message));
                }

                using (AmplaRepository<CycleModel> repository = new AmplaRepository<CycleModel>(client, credentials))
                {
                    var problems = repository.ValidateMapping(new CycleModel());
                    messages.AddRange(problems.Select(message => "Cycle: " + message));
                }

                return messages;
            }
        }
    }
}