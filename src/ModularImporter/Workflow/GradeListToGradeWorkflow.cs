﻿using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class GradeListToGradeWorkflow : MigrateWorkflow<GradeList,GradeModel>
    {
        private readonly IDataResolver dataResolver;

        public GradeListToGradeWorkflow(IDataResolver dataResolver)
        {
            this.dataResolver = dataResolver;
        }

        protected override bool TryMigrate(GradeList from, GradeModel to)
        {
            int shiftIndex = from.ShiftIndex;
            ShiftData shiftData = dataResolver.LookupShiftData(shiftIndex);
            
            if (shiftData != null)
            {
                to.SamplePeriod = shiftData.ShiftStart.AddSeconds(0);
                to.Crew = shiftData.Crew;
                to.Shift = shiftData.Shift;
            }

            to.Grade = from.GradeId;

            int loadId = from.LoadNo;
            LoadData loadData = dataResolver.LookupLoadData(loadId);
            if (loadData != null)
            {
                to.Material = loadData.LoadName;
            }

            to.Alumina = from.Grade00;
            to.Silica = from.Grade01;

            return shiftData != null && loadData != null;
        }
    }
}