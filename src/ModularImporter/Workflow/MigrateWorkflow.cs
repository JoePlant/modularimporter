﻿namespace ModularImporter.Workflow
{
    public abstract class MigrateWorkflow<TFrom, TTo> where TTo : new()
    {
        protected TTo Create()
        {
            return new TTo();
        }

        protected abstract bool TryMigrate(TFrom from, TTo to);
        
        public TTo Migrate(TFrom from)
        {
            TTo to = Create();
            if (TryMigrate(from, to))
            {
                return to;
            }
            return default(TTo);
        }
    }
}