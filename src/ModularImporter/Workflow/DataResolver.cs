﻿using System.Collections.Generic;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class DataResolver : IDataResolver
    { 
        private readonly List<ExpRoot> listExpRoot;
        private readonly Dictionary<int, ShiftData> resolvedShifts = new Dictionary<int, ShiftData>();
        private readonly List<EqmtList> listEquipList;
        private readonly Dictionary<string, EquipmentData> resolvedEquipment = new Dictionary<string, EquipmentData>();
        private readonly List<OperList> listOperList;
        private readonly Dictionary<string, OperatorData> resolvedOperators = new Dictionary<string, OperatorData>();

        private readonly List<Enums> listEnums;
        private readonly Dictionary<int, StatusData> resolvedStatus = new Dictionary<int, StatusData>();
        private readonly Dictionary<int, CategoryData> resolvedCategory = new Dictionary<int, CategoryData>();
        private readonly List<ReasonTable> listReasonTable;
        private readonly Dictionary<string, ReasonData> resolvedReasonData = new Dictionary<string, ReasonData>();
        private readonly Dictionary<int, LoadData> resolvedLoadData = new Dictionary<int, LoadData>(); 

        public DataResolver(List<ExpRoot> listExpRoot, List<EqmtList> listEquipList, List<OperList> listOperList, List<Enums> listEnums, List<ReasonTable> listReasonTable )
        {
            this.listExpRoot = listExpRoot;
            this.listEquipList = listEquipList;
            this.listOperList = listOperList;
            this.listEnums = listEnums;
            this.listReasonTable = listReasonTable;
        }

        public ShiftData LookupShiftData(int shiftIndex)
        {
            ShiftData shiftData;
            if (!resolvedShifts.TryGetValue(shiftIndex, out shiftData))
            {
                ExpRoot found = listExpRoot.Find(e => e.ShiftIndex == shiftIndex);
                if (found != null)
                {
                    shiftData = found.CreateShiftData();
                }
                resolvedShifts[shiftIndex] = shiftData;
            }

            return shiftData;
        }

        public EquipmentData LookupEquipmentData(string equipmentId)
        {
            EquipmentData equipmentData;
            if (!resolvedEquipment.TryGetValue(equipmentId, out equipmentData))
            {
                EqmtList found = listEquipList.Find(e => e.EqmtId == equipmentId);
                if (found != null)
                {
                    equipmentData = found.CreateEquipmentData();
                }
                resolvedEquipment[equipmentId] = equipmentData;
            }

            return equipmentData;
        }

        public OperatorData LookupOperatorData(int shiftIndex, string operatorId)
        {
            OperatorData operatorData;
            string key = shiftIndex + "-" + operatorId;
            if (!resolvedOperators.TryGetValue(key, out operatorData))
            {
                OperList found = listOperList.Find(e => e.ShiftIndex == shiftIndex && e.OperId == operatorId);
                if (found != null)
                {
                    operatorData = found.CreateOperatorData();
                }
                resolvedOperators[key] = operatorData;
            }

            return operatorData;
        }

        public OperatorData LookupOperatorData(string operatorId)
        {
            OperatorData operatorData;
            string key = operatorId;
            if (!resolvedOperators.TryGetValue(key, out operatorData))
            {
                OperList found = listOperList.Find(e => e.OperId == operatorId);
                if (found != null)
                {
                    operatorData = found.CreateOperatorData();
                }
                resolvedOperators[key] = operatorData;
            }

            return operatorData;
        }

        public StatusData LookupStatusData(int statusCode)
        {
            StatusData statusData;
            if (!resolvedStatus.TryGetValue(statusCode, out statusData))
            {
                Enums found = listEnums.Find(e => (e.EnumName == "STATUS") && (e.Num == statusCode));
                if (found != null)
                {
                    statusData = new StatusData
                        {
                            StatusCode = statusCode,
                            StatusName = found.Name
                        };
                }
                resolvedStatus[statusCode] = statusData;
            }

            return statusData;
        }

        public CategoryData LookupCategoryData(int categoryCode)
        {
            CategoryData categoryData;
            if (!resolvedCategory.TryGetValue(categoryCode, out categoryData))
            {
                Enums found = listEnums.Find(e => (e.EnumName == "TMCAT") && (e.Num == categoryCode));
                if (found != null)
                {
                    categoryData = new CategoryData
                    {
                        CategoryCode = categoryCode,
                        CategoryName = found.Name
                    };
                }
                resolvedCategory[categoryCode] = categoryData;
            }

            return categoryData;
        }

        public ReasonData LookupReasonData(int categoryCode, int statusCode, int reasonCode)
        {
            ReasonData reasonData;
            string key = categoryCode + "-" + statusCode + "-" + reasonCode;
            if (!resolvedReasonData.TryGetValue(key, out reasonData))
            {
                ReasonTable found = listReasonTable.Find(e => e.Reason == reasonCode && e.Category == categoryCode && e.Status == statusCode);
                if (found != null)
                {
                    reasonData = new ReasonData
                        {
                            CategoryCode = categoryCode,
                            StatusCode = statusCode,
                            ReasonCode = reasonCode,
                            ReasonName = found.Name
                        };
                }
                resolvedReasonData[key] = reasonData;
            }

            return reasonData;
        }

        public LoadData LookupLoadData(int loadCode)
        {
            LoadData loadData;
            if (!resolvedLoadData.TryGetValue(loadCode, out loadData))
            {
                Enums found = listEnums.Find(e => (e.EnumName == "LOAD") && (e.Num == loadCode));
                if (found != null)
                {
                    loadData = new LoadData
                    {
                        LoadCode = loadCode,
                        LoadName = found.Name
                    };
                }
                resolvedLoadData[loadCode] = loadData;
            }

            return loadData;
        }
    }
}