﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class LoadsToCycleModelWorkflow : MigrateWorkflow<Loads, CycleModel>
    {
        private readonly IDataResolver dataResolver;

        public LoadsToCycleModelWorkflow(IDataResolver dataResolver)
        {
            this.dataResolver = dataResolver;
        }

        protected override bool TryMigrate(Loads from, CycleModel to)
        {
            to.Count = 1;
            to.DumpKey = from.DumpRec;
            to.ShiftKey = from.ShiftIndex;
            to.LoadKey = from.DdbKey;

            int shiftIndex = from.ShiftIndex;
            ShiftData shiftData = dataResolver.LookupShiftData(shiftIndex);

            if (shiftData != null)
            {
                to.ArriveLoad = shiftData.ShiftStart.AddSeconds(from.TimeArrive);
                to.LoadSpotStart = shiftData.ShiftStart.AddSeconds(from.BeginSpot);
                to.LoadStart = shiftData.ShiftStart.AddSeconds(from.TimrLoad);
                to.LoadEnd = shiftData.ShiftStart.AddSeconds(from.TimeFull);

                to.EmptyHaulDuration = TimeSpan.FromSeconds(from.EmptyHaul);
                to.LoadQueueDuration = TimeSpan.FromSeconds(from.QueueTime);
                to.LoadSpotDuration = TimeSpan.FromSeconds(from.SpotTime);
                to.LoadDuration = TimeSpan.FromSeconds(from.LoadingTim);
                to.FullHaulDuration = TimeSpan.FromSeconds(from.FullHaul);

                to.CycleStart = to.ArriveLoad.Subtract(to.EmptyHaulDuration);

                //to.SamplePeriod = shiftData.ShiftStart.AddSeconds(from.TimeEmpty);
                //to.ArriveDump = shiftData.ShiftStart.AddSeconds(from.TimeArrive);
                //to.DumpStart = shiftData.ShiftStart.AddSeconds(from.TimeDump);
                //to.DumpEnd = shiftData.ShiftStart.AddSeconds(from.TimeEmpty);

                //to.DumpDuration = TimeSpan.FromSeconds(from.DumpingTim);
                //to.DumpQueueDuration = TimeSpan.FromSeconds(from.IdleTime);
            }

            {
                string equipmentId = from.Truck;
                EquipmentData equipmentData = dataResolver.LookupEquipmentData(equipmentId);
                if (equipmentData != null)
                {
                    to.Truck = equipmentData.EqmtId;
                    to.TruckModel = equipmentData.EquipmentModel;
                }
                else
                {
                    to.Truck = from.Truck;
                    to.TruckModel = "Unknown";
                }
            }

            {
                string equipmentId = from.Excav;
                EquipmentData equipmentData = dataResolver.LookupEquipmentData(equipmentId);
                if (equipmentData != null)
                {
                    to.Excavator = equipmentData.EqmtId;
                    to.ExcavatorModel = equipmentData.EquipmentModel;
                }
                else
                {
                    to.Excavator = from.Excav;
                    to.ExcavatorModel = "Unknown";
                }
            }

            string operatorId = from.Oper;
            OperatorData operatorData = dataResolver.LookupOperatorData(shiftIndex, operatorId);
            if (operatorData != null)
            {
                to.TruckOperator = operatorData.OperatorName;
            }
            else
            {
                operatorData = dataResolver.LookupOperatorData(operatorId);
                to.TruckOperator = operatorData != null ? operatorData.OperatorName : operatorId;
            }

            operatorId = from.EOper;
            operatorData = dataResolver.LookupOperatorData(shiftIndex, operatorId);
            if (operatorData != null)
            {
                to.ExcavatorOperator = operatorData.OperatorName;
            }
            else
            {
                operatorData = dataResolver.LookupOperatorData(operatorId);
                to.ExcavatorOperator = operatorData != null ? operatorData.OperatorName : operatorId;
            }

            to.Tonnes = from.LoadTons;
            //to.Destination = from.;
            to.Source = from.Loc;

            int loadCode = from.Load;
            LoadData loadData = dataResolver.LookupLoadData(loadCode);
            to.Material = (loadData != null) ? loadData.LoadName : "" + loadCode;

            to.Grade = from.Grade;

            return shiftData != null;
        }
    }
}