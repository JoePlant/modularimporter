﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class StatusEventToDowntimeWorkflow : MigrateWorkflow<StatusEvent,DowntimeModel>
    {
        private readonly IDataResolver dataResolver;

        public StatusEventToDowntimeWorkflow(IDataResolver dataResolver)
        {
            this.dataResolver = dataResolver;
        }

        protected override bool TryMigrate(StatusEvent from, DowntimeModel to)
        {
            to.RecordKey = from.DdbKey;
            to.Explanation = string.Format("Modular Category: {0} Status: {1} Reason: {2}", from.Category, from.Status, from.Reason);

            int shiftIndex = from.ShiftIndex;
            ShiftData shiftData = dataResolver.LookupShiftData(shiftIndex);
            
            if (shiftData != null)
            {
                to.StartTime = shiftData.ShiftStart.AddSeconds(from.StartTime);
                to.EndTime = shiftData.ShiftStart.AddSeconds(from.EndTime);
                to.Crew = shiftData.Crew;
                to.Shift = shiftData.Shift;
            }

            string equipmentId = from.Eqmt;
            EquipmentData equipmentData = dataResolver.LookupEquipmentData(equipmentId);
            if (equipmentData != null)
            {
                to.Equipment = equipmentData.EqmtId;
                to.EquipmentGroup = equipmentData.EquipmentGroup;
                to.EquipmentModel = equipmentData.EquipmentModel;
            }

            string operatorId = from.OperId;
            OperatorData operatorData = dataResolver.LookupOperatorData(shiftIndex, operatorId);
            if (operatorData != null)
            {
                to.Operator = operatorData.OperatorName;
                to.Crew = operatorData.Crew;
            }
            else
            {
                operatorData = dataResolver.LookupOperatorData(operatorId);
                to.Operator = operatorData != null ? operatorData.OperatorName : operatorId;
            }

            int statusCode = from.Status;
            StatusData statusData = dataResolver.LookupStatusData(statusCode);
            to.ModularStatus = statusData != null ? statusData.StatusName : Convert.ToString(statusCode);

            int categoryCode = from.Category;
            CategoryData categoryData = dataResolver.LookupCategoryData(categoryCode);
            to.ModularCategory = categoryData != null ? categoryData.CategoryName : Convert.ToString(categoryCode);

            int reasonCode = from.Reason;
            ReasonData reasonData = dataResolver.LookupReasonData(categoryCode, statusCode, reasonCode);
            to.ModularReason = reasonData != null ? reasonData.ReasonName : Convert.ToString(reasonCode);

            return shiftData != null && equipmentData != null;
        }
    }
}