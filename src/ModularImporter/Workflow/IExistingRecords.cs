﻿namespace ModularImporter.Workflow
{
    public interface IExistingRecords<in TModel>
    {
        /// <summary>
        /// Gets the count of existing records
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        int Count { get; }

        /// <summary>
        /// Gets the existing Ampla record id for the model if it exists
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="recordId">The record identifier.</param>
        /// <returns></returns>
        bool TryGetRecordId(TModel model, out int recordId);

        /// <summary>
        ///     Adds the specified model to the existing records.
        /// </summary>
        /// <param name="model">The model.</param>
        void Add(TModel model);
    }
}