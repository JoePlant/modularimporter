﻿using System;
using AmplaData;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;

namespace ModularImporter.Workflow
{
    public class ImportRecords<TFrom, TModel, TQuery> : IDisposable 
        where TFrom : DbModel
        where TModel : TQuery, new() 
        where TQuery : ModularModel, new()
    {
        private readonly ExistingRecords<TQuery> existingRecords;
        private readonly MigrateWorkflow<TFrom, TModel> migrateWorkflow;
        private IRepository<TModel> repository;

        public ImportRecords(MigrateWorkflow<TFrom, TModel> migrateWorkflow, IAmplaFactory amplaFactory)
        {
            this.migrateWorkflow = migrateWorkflow;
            ImportedCount = 0;

            existingRecords = new ExistingRecords<TQuery>(amplaFactory);
            existingRecords.Execute();

            repository = amplaFactory.GetRepository<TModel>();
        }

        public bool UpdateExistingRecords { get; set; }

        public int ImportedCount { get; private set; }

        public int UpdatedCount { get; private set; }

        public void Import(TFrom fromModel)
        {
            TModel model = migrateWorkflow.Migrate(fromModel);
            if (model != null)
            {
                int recordId;
                bool existing = existingRecords.TryGetRecordId(model, out recordId);

                if (!existing)
                {
                    ImportedCount++;
                    repository.Add(model);
                    existingRecords.Add(model);

                    Console.WriteLine("Added Id:{0}", model.Id);
                }
                else
                {
                    if (UpdateExistingRecords)
                    {
                        model.Id = recordId;
                        UpdatedCount++;
                        repository.Update(model);
                        Console.WriteLine("Updated Id:{0}", model.Id);
                    }
                }
            }
        }

        public void Dispose()
        {
            if (repository != null)
            {
                repository.Dispose();
                repository = null;
            }
        }
    }

}