﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class LoadsToLoadModelWorkflow : MigrateWorkflow<Loads, LoadModel>
    {
        private readonly IDataResolver dataResolver;

        public LoadsToLoadModelWorkflow(IDataResolver dataResolver)
        {
            this.dataResolver = dataResolver;
        }
        
        protected override bool TryMigrate(Loads @from, LoadModel to)
        {
            to.RecordKey = from.DdbKey;

            int shiftIndex = from.ShiftIndex;
            ShiftData shiftData = dataResolver.LookupShiftData(shiftIndex);

            if (shiftData != null)
            {
                to.Duration = TimeSpan.FromSeconds(from.TimeFull - from.TimeArrive);
                to.SamplePeriod = shiftData.ShiftStart.AddSeconds(from.TimeFull);
                to.Crew = shiftData.Crew;
                to.Shift = shiftData.Shift;
            }

            string truckOper = from.Oper;
            OperatorData od = dataResolver.LookupOperatorData(shiftIndex, truckOper);
            to.TruckOperator = od != null ? od.OperatorName : truckOper;

            string excOper = from.EOper;
            od = dataResolver.LookupOperatorData(shiftIndex, excOper);
            to.ExcavatorOperator = od != null ? od.OperatorName : excOper;

            to.Truck = from.Truck;
            to.Excavator = from.Excav;
            to.Grade = from.Grade;
            to.Tonnes = from.LoadTons;

            int loadCode = from.Load;
            LoadData loadData = dataResolver.LookupLoadData(loadCode);
            to.Material = (loadData != null) ? loadData.LoadName : "" + loadCode;

            to.Blast = from.Loc;
            to.Source = from.Loc;

            return shiftData != null;
        }
    }
}