﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class DumpsToDumpsModelWorkflow : MigrateWorkflow<Dumps,DumpModel>
    {
        private readonly IDataResolver dataResolver;

        public DumpsToDumpsModelWorkflow(IDataResolver dataResolver)
        {
            this.dataResolver = dataResolver;
        }

        protected override bool TryMigrate(Dumps from, DumpModel to)
        {
            to.RecordKey = from.DdbKey;
            
            int shiftIndex = from.ShiftIndex;
            ShiftData shiftData = dataResolver.LookupShiftData(shiftIndex);
            
            if (shiftData != null)
            {
                to.Duration = TimeSpan.FromSeconds(from.TimeEmpty - from.TimeArrive);
                to.SamplePeriod = shiftData.ShiftStart.AddSeconds(from.TimeEmpty);
                to.Crew = shiftData.Crew;
                to.Shift = shiftData.Shift;
            }

            to.Truck = from.Truck;
            to.Excavator = from.Excav;

            string operatorId = from.Oper;
            OperatorData operatorData = dataResolver.LookupOperatorData(shiftIndex, operatorId);
            if (operatorData != null)
            {
                to.TruckOperator = operatorData.OperatorName;
            }
            else
            {
                operatorData = dataResolver.LookupOperatorData(operatorId);
                to.TruckOperator = operatorData != null ? operatorData.OperatorName : operatorId;
            }

            operatorId = from.EOper;
            operatorData = dataResolver.LookupOperatorData(shiftIndex, operatorId);
            if (operatorData != null)
            {
                to.ExcavatorOperator = operatorData.OperatorName;
            }
            else
            {
                operatorData = dataResolver.LookupOperatorData(operatorId);
                to.ExcavatorOperator = operatorData != null ? operatorData.OperatorName : operatorId;
            }

            to.Tonnes = from.DumpTons;
            to.Source = from.Loc;
            to.Blast = from.Blast;

            int loadCode = from.Load;
            LoadData loadData = dataResolver.LookupLoadData(loadCode);
            to.Material = (loadData != null) ? loadData.LoadName : "" + loadCode;

            to.Grade = from.Grade;

            return shiftData != null;
        }
    }
}