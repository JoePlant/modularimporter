﻿using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public interface IDataResolver
    {
        ShiftData LookupShiftData(int shiftIndex);

        EquipmentData LookupEquipmentData(string equipmentId);

        OperatorData LookupOperatorData(int shiftIndex, string operatorId);

        OperatorData LookupOperatorData(string operatorId);

        StatusData LookupStatusData(int statusCode);

        CategoryData LookupCategoryData(int categoryCode);

        ReasonData LookupReasonData(int categoryCode, int statusCode, int reasonCode);

        LoadData LookupLoadData(int loadCode);
    }
}