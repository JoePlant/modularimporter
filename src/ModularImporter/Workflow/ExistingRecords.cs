﻿using System;
using System.Collections.Generic;
using AmplaData;
using ModularImporter.Models.Ampla;

namespace ModularImporter.Workflow
{
    public class ExistingRecords<TModel> : IExistingRecords<TModel> where TModel : ModularModel, new()
    {
        private readonly Dictionary<string, TModel> existingRecords;
        private readonly IAmplaFactory amplaFactory;
        private bool executed = false;

        public ExistingRecords(IAmplaFactory amplaFactory)
        {
            this.amplaFactory = amplaFactory;
            existingRecords = new Dictionary<string, TModel>();
        }

        public int Count
        {
            get { return existingRecords.Count; }
        }

        public bool TryGetRecordId(TModel model, out int recordId)
        {
            if (model != null)
            {
                string key = model.UniqueKey();
                TModel record;
                if (existingRecords.TryGetValue(key, out record))
                {
                    recordId = record.Id;
                    return true;
                }
            }
            recordId = 0;
            return false;
        }

        /// <summary>
        /// Finds the existing records.
        /// </summary>
        /// <returns></returns>
        public void Execute()
        {
            if (executed)
            {
                throw new InvalidOperationException("Only Execute once.");
            }

            executed = true;
            using (IRepository<TModel> repository = amplaFactory.GetRepository<TModel>())
            {
                foreach (TModel queryModel in repository.GetAll())
                {
                    string key = queryModel.UniqueKey();
                    existingRecords[key] = queryModel;
                }
            }
        }

        public void Add(TModel model)
        {
            string key = model.UniqueKey();
            existingRecords[key] = model;
        }
    }
}