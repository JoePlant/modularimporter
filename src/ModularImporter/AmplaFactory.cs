﻿using AmplaData;
using AmplaData.AmplaData2008;
using AmplaData.AmplaRepository;

namespace ModularImporter
{
    public class AmplaFactory : IAmplaFactory
    {
        private readonly ICredentialsProvider credentialsProvider;
        private readonly IDataWebServiceClient webServiceClient;

        public AmplaFactory(IDataWebServiceClient webServiceClient, ICredentialsProvider credentialsProvider)
        {
            this.webServiceClient = webServiceClient;
            this.credentialsProvider = credentialsProvider;
        }

        public IRepository<TModel> GetRepository<TModel>() where TModel : class, new()
        {
            return new AmplaRepository<TModel>(webServiceClient, credentialsProvider);
        }
    }
}