﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class LoadsClassMap : CsvClassMap<Loads>
    {
        public LoadsClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.Truck).Index(3);
            Map(m => m.Excav).Index(4);
            Map(m => m.Grade).Index(5);
            Map(m => m.Loc).Index(6);
            Map(m => m.DumpRec).Index(7);
            Map(m => m.MeasureTon).Index(8);
            Map(m => m.TimeArrive).Index(9);
            Map(m => m.TimrLoad).Index(10);
            Map(m => m.TimeFull).Index(11);
            Map(m => m.Ctrteh).Index(12);
            Map(m => m.Ctrtfh).Index(13);
            Map(m => m.Atrteh).Index(14);
            Map(m => m.Atrtfh).Index(15);
            Map(m => m.Load).Index(16);
            Map(m => m.ExtraLoad).Index(17);
            Map(m => m.Lift_Up).Index(18);
            Map(m => m.Lift_Down).Index(19);
            Map(m => m.LiftDist_U).Index(20);
            Map(m => m.LiftDist_D).Index(21);
            Map(m => m.Disteh).Index(22);
            Map(m => m.Distfh).Index(23);
            Map(m => m.Efheh).Index(24);
            Map(m => m.Efhfh).Index(25);
            Map(m => m.Loadtype).Index(26);
            Map(m => m.LoadTons).Index(27);
            Map(m => m.ShiftLink).Index(28);
            Map(m => m.Oper).Index(29);
            Map(m => m.EOper).Index(30);
            Map(m => m.BeginSpot).Index(31);
            Map(m => m.QueueTime).Index(32);
            Map(m => m.SpotTime).Index(33);
            Map(m => m.LoadingTim).Index(34);
            Map(m => m.FullHaul).Index(35);
            Map(m => m.DumpTime).Index(36);
            Map(m => m.EmptyHaul).Index(37);
            Map(m => m.HangTime).Index(38);
            Map(m => m.IdleTime).Index(39);
            Map(m => m.ExcavNextL).Index(40);
            Map(m => m.ExcavPrevL).Index(41);
            Map(m => m.LoadNumber).Index(42);
            Map(m => m.Val_sp).Index(43);
            Map(m => m.Val_ld).Index(44);
            Map(m => m.Val_mt).Index(45);
            Map(m => m.Val_spld).Index(46);
            Map(m => m.Val_spldmt).Index(47);
            Map(m => m.Val_ehaul).Index(48);
            Map(m => m.Val_fhaul).Index(49);
            Map(m => m.Ot).Index(50);
            Map(m => m.Ut).Index(51);
            Map(m => m.Isp).Index(52);
            Map(m => m.Ild).Index(53);
            Map(m => m.Ifh).Index(54);
            Map(m => m.Ieh).Index(55);
            Map(m => m.Dmp).Index(56);
            Map(m => m.Tk_TmCat00).Index(57);
            Map(m => m.Tk_TmCat01).Index(58);
            Map(m => m.Tk_TmCat02).Index(59);
            Map(m => m.Tk_TmCat03).Index(60);
            Map(m => m.Tk_TmCat04).Index(61);
            Map(m => m.Tk_TmCat05).Index(62);
            Map(m => m.Tk_TmCat06).Index(63);
            Map(m => m.Tk_TmCat07).Index(64);
            Map(m => m.Tk_TmCat08).Index(65);
            Map(m => m.Tk_TmCat09).Index(66);
            Map(m => m.Tk_TmCat10).Index(67);
            Map(m => m.Tk_TmCat11).Index(68);
            Map(m => m.Tk_TmCat12).Index(69);
            Map(m => m.Tk_TmCat13).Index(70);
            Map(m => m.Tk_TmCat14).Index(71);
            Map(m => m.Tk_TmCat15).Index(72);
            Map(m => m.Tk_TmCat16).Index(73);
            Map(m => m.Tk_TmCat17).Index(74);
            Map(m => m.Tk_TmCat18).Index(75);
            Map(m => m.Tk_TmCat19).Index(76);
            Map(m => m.Ex_TmCat00).Index(77);
            Map(m => m.Ex_TmCat01).Index(78);
            Map(m => m.Ex_TmCat02).Index(79);
            Map(m => m.Ex_TmCat03).Index(80);
            Map(m => m.Ex_TmCat04).Index(81);
            Map(m => m.Ex_TmCat05).Index(82);
            Map(m => m.Ex_TmCat06).Index(83);
            Map(m => m.Ex_TmCat07).Index(84);
            Map(m => m.Ex_TmCat08).Index(85);
            Map(m => m.Ex_TmCat09).Index(86);
            Map(m => m.Ex_TmCat10).Index(87);
            Map(m => m.Ex_TmCat11).Index(88);
            Map(m => m.Ex_TmCat12).Index(89);
            Map(m => m.Ex_TmCat13).Index(90);
            Map(m => m.Ex_TmCat14).Index(91);
            Map(m => m.Ex_TmCat15).Index(92);
            Map(m => m.Ex_TmCat16).Index(93);
            Map(m => m.Ex_TmCat17).Index(94);
            Map(m => m.Ex_TmCat18).Index(95);
            Map(m => m.Ex_TmCat19).Index(96);
            Map(m => m.Hos).Index(97);
            Map(m => m.Intvl).Index(98);

        }
    }
}