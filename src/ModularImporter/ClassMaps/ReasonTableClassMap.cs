﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class ReasonTableClassMap : CsvClassMap<ReasonTable>
    {
        public ReasonTableClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2).ConvertUsing(row =>
                {
                    int dbbKey;
                    if (!row.TryGetField(2, out dbbKey))
                    {
                        dbbKey = 0;
                    }
                    return dbbKey;
                });
            Map(m => m.Name).Index(3);
            Map(m => m.Reason).Index(4);
            Map(m => m.Status).Index(5);
            Map(m => m.DelayTime).Index(6);
            Map(m => m.Category).Index(7);
            Map(m => m.MainTime).Index(8);
        }
    }
}