﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class GradeListClassMap : CsvClassMap<GradeList>
    {
        public GradeListClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.GradeId).Index(3);
            Map(m => m.Loc).Index(4);
            Map(m => m.Dump).Index(5);
            Map(m => m.Inv).Index(6);
            Map(m => m.Grade00).Index(7);
            Map(m => m.Grade01).Index(8);
            Map(m => m.Grade02).Index(9);
            Map(m => m.Grade03).Index(10);
            Map(m => m.Grade04).Index(11);
            Map(m => m.Grade05).Index(12);
            Map(m => m.Grade06).Index(13);
            Map(m => m.Grade07).Index(14);
            Map(m => m.Grade08).Index(15);
            Map(m => m.Grade09).Index(16);
            Map(m => m.Grade10).Index(17);
            Map(m => m.Grade11).Index(18);
            Map(m => m.Grade12).Index(19);
            Map(m => m.Grade13).Index(20);
            Map(m => m.Grade14).Index(21);
            Map(m => m.Grade15).Index(22);
            Map(m => m.Grade16).Index(23);
            Map(m => m.Grade17).Index(24);
            Map(m => m.Grade18).Index(25);
            Map(m => m.Grade19).Index(26);
            Map(m => m.Grade20).Index(27);
            Map(m => m.Grade21).Index(28);
            Map(m => m.Grade22).Index(29);
            Map(m => m.Grade23).Index(30);
            Map(m => m.Grade24).Index(31);
            Map(m => m.Spgr).Index(32);
            Map(m => m.LoadNo).Index(33);
            Map(m => m.Load).Index(34);
            Map(m => m.BlendNo).Index(35);
            Map(m => m.Blend).Index(36);

        }
    }
}