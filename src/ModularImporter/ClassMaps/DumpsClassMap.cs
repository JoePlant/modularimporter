﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class DumpsClassMap : CsvClassMap<Dumps>
    {
        public DumpsClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.Truck).Index(3);
            Map(m => m.Loc).Index(4);
            Map(m => m.Grade).Index(5);
            Map(m => m.Excav).Index(6);
            Map(m => m.Blast).Index(7);
            Map(m => m.Bay).Index(8);
            Map(m => m.LoadRec).Index(9);
            Map(m => m.MeasureTon).Index(10);
            Map(m => m.TimeArrive).Index(11);
            Map(m => m.TimeDump).Index(12);
            Map(m => m.TimeEmpty).Index(13);
            Map(m => m.TimeDigest).Index(14);
            Map(m => m.CalcTravTi).Index(15);
            Map(m => m.Load).Index(16);
            Map(m => m.ExtraLoad).Index(17);
            Map(m => m.LiftUp).Index(18);
            Map(m => m.LiftDown).Index(19);
            Map(m => m.LiftDistU).Index(20);
            Map(m => m.LiftDistD).Index(21);
            Map(m => m.Dist).Index(22);
            Map(m => m.Efh).Index(23);
            Map(m => m.LoadType).Index(24);
            Map(m => m.DumpTons).Index(25);
            Map(m => m.ShiftLink).Index(26);
            Map(m => m.DumpId).Index(27);
            Map(m => m.Oper).Index(28);
            Map(m => m.EOper).Index(29);
            Map(m => m.IdleTime).Index(30);
            Map(m => m.LoadNumber).Index(31);
            Map(m => m.DumpingTim).Index(32);
            Map(m => m.ValDmp).Index(33);
            Map(m => m.IDmp).Index(34);
            Map(m => m.Hos).Index(35);
            Map(m => m.Intvl).Index(36);
        }
    }
}