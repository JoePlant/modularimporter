﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class ExpRootClassMap : CsvClassMap<ExpRoot>
    {
        public ExpRootClassMap()
        {
            Map(m => m.ShiftDate).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.ShiftIndex).Index(3);
            Map(m => m.Name).Index(4);
            Map(m => m.Year).Index(5);
            Map(m => m.MonthNo).Index(6);
            Map(m => m.Month).Index(7);
            Map(m => m.Day).Index(8);
            Map(m => m.ShiftId).Index(9);
            Map(m => m.Shift).Index(10);
            Map(m => m.Date).Index(11);
            Map(m => m.Start).Index(12);
            Map(m => m.CrewId).Index(13);
            Map(m => m.Crew).Index(14);
            Map(m => m.Len).Index(15);
            Map(m => m.DispTime).Index(16);
            Map(m => m.Holiday).Index(17);
            Map(m => m.DdMmYy).Index(18);
            Map(m => m.FinYear).Index(19);
            Map(m => m.FinQuarter).Index(20);
            Map(m => m.FinMonth).Index(21);
            Map(m => m.FinWeek).Index(22);             
        }
    }
}