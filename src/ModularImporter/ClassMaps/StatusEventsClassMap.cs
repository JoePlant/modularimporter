﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class StatusEventsClassMap : CsvClassMap<StatusEvent>
    {
        public StatusEventsClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.Eqmt).Index(3);
            Map(m => m.Unit).Index(4);
            Map(m => m.OperId).Index(5);
            Map(m => m.StartTime).Index(6);
            Map(m => m.EndTime).Index(7);
            Map(m => m.Duration).Index(8);
            Map(m => m.Reason).Index(9);
            Map(m => m.Status).Index(10);
            Map(m => m.Category).Index(11);
            Map(m => m.Comment).Index(12);
            Map(m => m.VEvent).Index(13);
            Map(m => m.ReasonLink).Index(14);
        }
    }
}