﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class OperListClassMap : CsvClassMap<OperList>
    {
        public OperListClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.OperId).Index(3);
            Map(m => m.Name).Index(4);
            Map(m => m.CrewNo).Index(5);
            Map(m => m.Crew).Index(6);
        }
    }
}