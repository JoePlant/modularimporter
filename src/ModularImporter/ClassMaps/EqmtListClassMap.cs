﻿using CsvHelper.Configuration;
using ModularImporter.Models.Modular;

namespace ModularImporter.ClassMaps
{
    public sealed class EqmtListClassMap : CsvClassMap<EqmtList>
    {
        public EqmtListClassMap()
        {
            Map(m => m.ShiftIndex).Index(0);
            Map(m => m.CliId).Index(1);
            Map(m => m.DdbKey).Index(2);
            Map(m => m.EqmtId).Index(3);
            Map(m => m.Pit).Index(4);
            Map(m => m.Size).Index(5);
            Map(m => m.UnitNo).Index(6);
            Map(m => m.Unit).Index(7);
            Map(m => m.EqmtTypeNo).Index(8);
            Map(m => m.EqmtType).Index(9);
            Map(m => m.ExtraLoad).Index(10);
            Map(m => m.Trammer).Index(11);
            Map(m => m.Performance).Index(12);
            Map(m => m.OperId).Index(13);
            Map(m => m.OperName).Index(14);
        }
    }
}