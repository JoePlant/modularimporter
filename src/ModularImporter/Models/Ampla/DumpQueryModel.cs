﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Production")]
    [AmplaLocation("Enterprise.Site.Mining.Movements.Loads")]
    public class LoadQueryModel : ModularModel
    {
        [AmplaField("Sample Period")]
        public DateTime SamplePeriod { get; set; }

        [AmplaField("Duration")]
        public TimeSpan Duration { get; set; }

        [AmplaField("ModularKey")]
        public int RecordKey { get; set; }

        public override string UniqueKey()
        {
            return Convert.ToString(RecordKey);
        }
    }
}