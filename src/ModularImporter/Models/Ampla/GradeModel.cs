﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Quality")]
    [AmplaLocation("Enterprise.Site.Mining.Grade.Quality")]
    public class GradeModel : GradeQueryModel
    {
        [AmplaField("Shift")]
        public string Shift { get; set; }

        [AmplaField("Crew")]
        public string Crew { get; set; }

        [AmplaField("IsManual")]
        public bool IsManual { get; set; }

        [AmplaField("Material")]
        public string Material {get; set;}
        
        [AmplaField("Alumina")]
        public double Alumina { get; set; }

        [AmplaField("Silica")]
        public double Silica { get; set; }
    }
}