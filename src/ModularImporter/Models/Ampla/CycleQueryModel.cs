﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Production")]
    [AmplaLocation("Enterprise.Site.Mining.Cycles.Cycles")]
    public class CycleQueryModel : ModularModel
    {
        [AmplaField("Sample Period")]
        public DateTime SamplePeriod { get; set; }

        [AmplaField("Duration")]
        public TimeSpan Duration { get; set; }

        [AmplaField("DumpKey")]
        public int DumpKey { get; set; }

        [AmplaField("LoadKey")]
        public int LoadKey { get; set; }

        [AmplaField("ShiftKey")]
        public int ShiftKey { get; set; }

        public override string UniqueKey()
        {
            return string.Format("{0}-{1}-{2}", ShiftKey, DumpKey, LoadKey);
        }
    }
}