﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Quality")]
    [AmplaLocation("Enterprise.Site.Mining.Grade.Quality")]
    public class GradeQueryModel : ModularModel
    {
        [AmplaField("Sample Period")]
        public DateTime SamplePeriod { get; set; }

        [AmplaField("Duration")]
        public TimeSpan Duration { get; set; }

        [AmplaField("Grade")]
        public string Grade { get; set; }

        
        public override string UniqueKey()
        {
            return Grade;
        }
    }
}