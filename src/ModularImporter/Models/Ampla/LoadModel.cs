﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Production")]
    [AmplaLocation("Enterprise.Site.Mining.Movements.Loads")]
    public class LoadModel : LoadQueryModel
    {
        [AmplaField("IsManual")]
        public bool IsManual { get; set; }

        [AmplaField("Truck")]
        public string Truck { get; set; }

        [AmplaField("Excavator")]
        public string Excavator { get; set; }

        [AmplaField("Blast")]
        public string Blast { get; set; }

        [AmplaField("Material")]
        public string Material { get; set; }

        [AmplaField("Source")]
        public string Source { get; set; }

        [AmplaField("Grade")]
        public string Grade { get; set; }

        [AmplaField("Crew")]
        public string Crew { get; set; }

        [AmplaField("Shift")]
        public string Shift { get; set; }

        [AmplaField("Truck Operator")]
        public string TruckOperator { get; set; }

        [AmplaField("Excavator Operator")]
        public string ExcavatorOperator { get; set; }

        [AmplaField("Tonnes")]
        public double Tonnes { get; set; }
    }
}