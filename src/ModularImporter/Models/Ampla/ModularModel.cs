﻿using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    public abstract class ModularModel
    {
        public int Id { get; set; }



        public abstract string UniqueKey();
    }
}