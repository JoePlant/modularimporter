﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Downtime")]
    [AmplaLocation("Enterprise.Site.Mining.Fleet.Downtime")]
    public class DowntimeQueryModel : ModularModel
    {
        [AmplaField("Start Time")]
        public DateTime StartTime { get; set; }

        [AmplaField("End Time")]
        public DateTime EndTime { get; set; }

        [AmplaField("ModularKey")]
        public int RecordKey { get; set; }

        public override string UniqueKey()
        {
            return Convert.ToString(RecordKey);
        }
    }
}