﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Downtime")]
    [AmplaLocation("Enterprise.Site.Mining.Fleet.Downtime")]
    public class DowntimeModel : DowntimeQueryModel
    {
        [AmplaField("IsManual")]
        public bool IsManual { get; set; }

        [AmplaField("Equipment")]
        public string Equipment { get; set; }

        [AmplaField("Equipment Model")]
        public string EquipmentModel { get; set; }

        [AmplaField("Equipment Group")]
        public string EquipmentGroup { get; set; }
        
        [AmplaField("Operator")]
        public string Operator { get; set; }

        [AmplaField("Crew")]
        public string Crew { get; set; }

        [AmplaField("Shift")]
        public string Shift { get; set; }

        [AmplaField("Explanation")]
        public string Explanation { get; set; }

        [AmplaField("Modular Status")]
        public string ModularStatus { get; set; }

        [AmplaField("Modular Category")]
        public string ModularCategory { get; set; }

        [AmplaField("Modular Reason")]
        public string ModularReason { get; set; } 
    }
}