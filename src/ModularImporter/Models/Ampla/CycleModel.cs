﻿using System;
using AmplaData.Attributes;

namespace ModularImporter.Models.Ampla
{
    [AmplaModule("Production")]
    [AmplaLocation("Enterprise.Site.Mining.Cycles.Cycles")]
    public class CycleModel : CycleQueryModel
    {
        [AmplaField("IsManual")]
        public bool IsManual { get; set; }

        [AmplaField("Truck")]
        public string Truck { get; set; }

        [AmplaField("Truck Model")]
        public string TruckModel { get; set; }

        [AmplaField("Truck Operator")]
        public string TruckOperator { get; set; }

        [AmplaField("Excavator")]
        public string Excavator { get; set; }

        [AmplaField("Excavator Model")]
        public string ExcavatorModel { get; set; }

        [AmplaField("Excavator Operator")]
        public string ExcavatorOperator { get; set; }

        [AmplaField("Source")]
        public string Source { get; set; }

        [AmplaField("Destination")]
        public string Destination { get; set; }

        [AmplaField("Material")]
        public string Material { get; set; }

        [AmplaField("Grade")]
        public string Grade { get; set; }

        [AmplaField("Tonnes")]
        public double Tonnes { get; set; }

        [AmplaField("Count")]
        public int Count { get; set; }

        [AmplaField("Cycle Start")]
        public DateTime CycleStart { get; set; }

        [AmplaField("Arrive Load")]
        public DateTime ArriveLoad { get; set; }

        [AmplaField("Load Spot Start")]
        public DateTime LoadSpotStart { get; set; }

        [AmplaField("Load Start")]
        public DateTime LoadStart { get; set; }

        [AmplaField("Load End")]
        public DateTime LoadEnd { get; set; }

        [AmplaField("Arrive Dump")]
        public DateTime ArriveDump { get; set; }

        [AmplaField("Dump Start")]
        public DateTime DumpStart { get; set; }

        [AmplaField("Dump End")]
        public DateTime DumpEnd { get; set; }

        [AmplaField("Empty Haul Duration")]
        public TimeSpan EmptyHaulDuration { get; set; }

        [AmplaField("Load Queue Duration")]
        public TimeSpan LoadQueueDuration { get; set; }

        [AmplaField("Load Spot Duration")]
        public TimeSpan LoadSpotDuration { get; set; }

        [AmplaField("Load Duration")]
        public TimeSpan LoadDuration { get; set; }

        [AmplaField("Full Haul Duration")]
        public TimeSpan FullHaulDuration { get; set; }

        [AmplaField("Dump Queue Duration")]
        public TimeSpan DumpQueueDuration { get; set; }
            
        [AmplaField("Dump Duration")]
        public TimeSpan DumpDuration { get; set; }
    }
}