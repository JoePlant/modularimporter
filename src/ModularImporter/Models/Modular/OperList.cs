﻿using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Models.Modular
{
    public class OperList
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public int DdbKey { get; set; }
        public string OperId { get; set; }
        public string Name { get; set; }
        public int CrewNo { get; set; }
        public string Crew { get; set; }

        public OperatorData CreateOperatorData()
        {
            return new OperatorData()
            {
                ShiftIndex = ShiftIndex,
                OperatorId = OperId,
                OperatorName = Name,
                Crew = Crew
            };
        }
    }
}