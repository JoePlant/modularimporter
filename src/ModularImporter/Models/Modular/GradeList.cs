﻿namespace ModularImporter.Models.Modular
{
    public class GradeList : DbModel
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public string GradeId { get; set; }
        public string Loc { get; set; }
        public string Dump { get; set; }
        public double Inv { get; set; }
        public double Grade00 { get; set; }
        public double Grade01 { get; set; }
        public double Grade02 { get; set; }
        public double Grade03 { get; set; }
        public double Grade04 { get; set; }
        public double Grade05 { get; set; }
        public double Grade06 { get; set; }
        public double Grade07 { get; set; }
        public double Grade08 { get; set; }
        public double Grade09 { get; set; }
        public double Grade10 { get; set; }
        public double Grade11 { get; set; }
        public double Grade12 { get; set; }
        public double Grade13 { get; set; }
        public double Grade14 { get; set; }
        public double Grade15 { get; set; }
        public double Grade16 { get; set; }
        public double Grade17 { get; set; }
        public double Grade18 { get; set; }
        public double Grade19 { get; set; }
        public double Grade20 { get; set; }
        public double Grade21 { get; set; }
        public double Grade22 { get; set; }
        public double Grade23 { get; set; }
        public double Grade24 { get; set; }
        public double Spgr { get; set; }
        public int LoadNo { get; set; }
        public string Load { get; set; }
        public int BlendNo { get; set; }
        public int Blend { get; set; }

    }
}