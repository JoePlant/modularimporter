﻿namespace ModularImporter.Models.Modular
{
    public class StatusEvent : DbModel
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public string Eqmt { get; set; }
        public int Unit { get; set; }
        public string OperId { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public int Duration { get; set; }
        public int Reason { get; set; }
        public int Status { get; set; }
        public int Category { get; set; }
        public string Comment { get; set; }
        public int VEvent { get; set; }
        public int ReasonLink { get; set; }
    }
}