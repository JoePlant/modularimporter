﻿using System;

namespace ModularImporter.Models.Modular
{
    public class Loads : DbModel
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public string Truck { get; set; }
        public string Excav { get; set; }
        public string Grade { get; set; }
        public string Loc { get; set; }
        public int DumpRec { get; set; }
        public double MeasureTon { get; set; }
        public int TimeArrive { get; set; }     // time of arrival
        public int TimrLoad { get; set; }       // time of load start
        public int TimeFull { get; set; }       // time of load full
        public int Ctrteh { get; set; }         // calculated (expected) travel time to loading 
        public int Ctrtfh { get; set; }         // calculated (expected) travel time to dumping
        public int Atrteh { get; set; }         // actual travel time to loading
        public int Atrtfh { get; set; }         // actual travel time to dumping
        public int Load { get; set; }
        public int ExtraLoad { get; set; }
// ReSharper disable InconsistentNaming
        public int Lift_Up { get; set; }        // total change in elevation going uphill to load location
        public int Lift_Down { get; set; }      // total change in elevation going downhill to load location
        public int LiftDist_U { get; set; }     // total distance travelled going uphill to load location
        public int LiftDist_D { get; set; }     // total distance travelled going downhill to load location
// ReSharper restore InconsistentNaming
        public int Disteh { get; set; }         // total distance travelled from previous dump to load location
        public int Distfh { get; set; }         // total distance travelled from load location to dump
        public int Efheh { get; set; }
        public int Efhfh { get; set; }
        public int Loadtype { get; set; }
        public double LoadTons { get; set; }
        public int ShiftLink { get; set; }
        public string Oper { get; set; }
        public string EOper { get; set; }
        public int BeginSpot { get; set; }
        public int QueueTime { get; set; }
        public int SpotTime { get; set; }
        public int LoadingTim { get; set; }
        public int FullHaul { get; set; }
        public int DumpTime { get; set; }
        public int EmptyHaul { get; set; }
        public int HangTime { get; set; }
        public int IdleTime { get; set; }
        public int ExcavNextL { get; set; }
        public int ExcavPrevL { get; set; }
        public int LoadNumber { get; set; }
// ReSharper disable InconsistentNaming
        public int Val_sp { get; set; }
        public int Val_ld { get; set; }
        public int Val_mt { get; set; }
        public int Val_spld { get; set; }
        public int Val_spldmt { get; set; }
        public int Val_ehaul { get; set; }
        public int Val_fhaul { get; set; }
// ReSharper restore InconsistentNaming
        public int Ot { get; set; }
        public int Ut { get; set; }
        public int Isp { get; set; }
        public int Ild { get; set; }
        public int Ifh { get; set; }
        public int Ieh { get; set; }
        public int Dmp { get; set; }
// ReSharper disable InconsistentNaming
        public int Tk_TmCat00 { get; set; }
        public int Tk_TmCat01 { get; set; }
        public int Tk_TmCat02 { get; set; }
        public int Tk_TmCat03 { get; set; }
        public int Tk_TmCat04 { get; set; }
        public int Tk_TmCat05 { get; set; }
        public int Tk_TmCat06 { get; set; }
        public int Tk_TmCat07 { get; set; }
        public int Tk_TmCat08 { get; set; }
        public int Tk_TmCat09 { get; set; }
        public int Tk_TmCat10 { get; set; }
        public int Tk_TmCat11 { get; set; }
        public int Tk_TmCat12 { get; set; }
        public int Tk_TmCat13 { get; set; }
        public int Tk_TmCat14 { get; set; }
        public int Tk_TmCat15 { get; set; }
        public int Tk_TmCat16 { get; set; }
        public int Tk_TmCat17 { get; set; }
        public int Tk_TmCat18 { get; set; }
        public int Tk_TmCat19 { get; set; }
        public int Ex_TmCat00 { get; set; }
        public int Ex_TmCat01 { get; set; }
        public int Ex_TmCat02 { get; set; }
        public int Ex_TmCat03 { get; set; }
        public int Ex_TmCat04 { get; set; }
        public int Ex_TmCat05 { get; set; }
        public int Ex_TmCat06 { get; set; }
        public int Ex_TmCat07 { get; set; }
        public int Ex_TmCat08 { get; set; }
        public int Ex_TmCat09 { get; set; }
        public int Ex_TmCat10 { get; set; }
        public int Ex_TmCat11 { get; set; }
        public int Ex_TmCat12 { get; set; }
        public int Ex_TmCat13 { get; set; }
        public int Ex_TmCat14 { get; set; }
        public int Ex_TmCat15 { get; set; }
        public int Ex_TmCat16 { get; set; }
        public int Ex_TmCat17 { get; set; }
        public int Ex_TmCat18 { get; set; }
        public int Ex_TmCat19 { get; set; }
// ReSharper restore InconsistentNaming
        public int Hos { get; set; }
        public TimeSpan Intvl { get; set; }
 
    }
}