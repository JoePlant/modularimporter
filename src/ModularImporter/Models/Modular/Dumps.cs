﻿using System;

namespace ModularImporter.Models.Modular
{
    public class Dumps : DbModel
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public string Truck { get; set; }
        public string Loc { get; set; }
        public string Grade { get; set; }
        public string Excav { get; set; }
        public string Blast { get; set; }
        public string Bay { get; set; }
        public int LoadRec { get; set; }
        public double MeasureTon { get; set; }
        public int TimeArrive { get; set; }
        public int TimeDump { get; set; }
        public int TimeEmpty { get; set; }
        public int TimeDigest { get; set; }
        public int CalcTravTi { get; set; }
        public int Load { get; set; }
        public int ExtraLoad { get; set; }
        public int LiftUp { get; set; }
        public int LiftDown { get; set; }
        public int LiftDistU { get; set; }
        public int LiftDistD { get; set; }
        public int Dist { get; set; }
        public int Efh { get; set; }
        public int LoadType { get; set; }
        public double DumpTons { get; set; }
        public int ShiftLink { get; set; }
        public int DumpId { get; set; }
        public string Oper { get; set; }
        public string EOper { get; set; }
        public int IdleTime { get; set; }
        public int LoadNumber { get; set; }
        public int DumpingTim { get; set; }
        public int ValDmp { get; set; }
// ReSharper disable InconsistentNaming
        public int IDmp { get; set; }
// ReSharper restore InconsistentNaming
        public int Hos { get; set; }
        public TimeSpan Intvl { get; set; }
 
    }
}