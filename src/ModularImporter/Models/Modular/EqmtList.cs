﻿using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Models.Modular
{
    public class EqmtList
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public int DdbKey { get; set; }
        public string EqmtId { get; set; }
        public string Pit { get; set; }
        public double Size { get; set; }
        public int UnitNo {get; set;}
        public string Unit { get; set; }
        public int EqmtTypeNo {get; set;}
        public string EqmtType { get; set; }
        public int ExtraLoad { get; set; }
        public int Trammer { get; set; }
        public double Performance { get; set; }
        public string OperId { get; set; }
        public string OperName { get; set; }

        public EquipmentData CreateEquipmentData()
        {
            return new EquipmentData
                {
                    EqmtId = EqmtId, 
                    EquipmentGroup = Unit, 
                    EquipmentModel = EqmtType, 
                    Size = Size
                };
        }
    }
}