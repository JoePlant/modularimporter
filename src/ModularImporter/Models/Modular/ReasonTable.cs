﻿namespace ModularImporter.Models.Modular
{
    public class ReasonTable
    {
        public int ShiftIndex { get; set; }
        public int CliId { get; set; }
        public int DdbKey { get; set; }
        public string Name { get; set; }
        public int Reason { get; set; }
        public int Status { get; set; }
        public int DelayTime { get; set; }
        public int Category { get; set; }
        public string MainTime { get; set; }
    }
}