﻿using System;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Models.Modular
{
    public class ExpRoot
    {
        public DateTime ShiftDate { get; set; }
        public int CliId { get; set; }
        public int DdbKey { get; set; }
        public int ShiftIndex { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public int MonthNo { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
        public int ShiftId { get; set; }
        public string Shift { get; set; }
        public double Date { get; set; }
        public int Start { get; set; }
        public int CrewId { get; set; }
        public string Crew { get; set; }
        public int Len { get; set; }
        public int DispTime { get; set; }
        public int Holiday { get; set; }
        public string DdMmYy { get; set; }
        public int FinYear { get; set; }
        public int FinQuarter { get; set; }
        public int FinMonth { get; set; }
        public int FinWeek { get; set; }

        public ShiftData CreateShiftData()
        {
            return new ShiftData
                {
                    ShiftIndex = ShiftIndex,
                    ShiftStart = ShiftDate.AddSeconds(Start),
                    Shift = Shift,
                    Crew = Crew
                };
        }
    }
}