﻿namespace ModularImporter.Models.Modular
{
    public class Enums
    {
        public int CliId { get; set; }
        public string EnumName { get; set; }
        public int Num { get; set; }
        public string Name { get; set; }
        public string Abbrev { get; set; }
        public int Flags { get; set; }
    }
}