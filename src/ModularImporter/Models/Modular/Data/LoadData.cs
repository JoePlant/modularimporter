﻿namespace ModularImporter.Models.Modular.Data
{
    public class LoadData
    {
        public int LoadCode { get; set; }
        public string LoadName { get; set; }  
    }
}