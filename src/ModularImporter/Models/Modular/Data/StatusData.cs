﻿namespace ModularImporter.Models.Modular.Data
{
    public class StatusData
    {
        public int StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}