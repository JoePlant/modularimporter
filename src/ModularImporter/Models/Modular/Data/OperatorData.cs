﻿namespace ModularImporter.Models.Modular.Data
{
    public class OperatorData
    {
        public int ShiftIndex { get; set; }
        public string OperatorId { get; set; }
        public string OperatorName { get; set; }
        public string Crew { get; set; } 
    }
}