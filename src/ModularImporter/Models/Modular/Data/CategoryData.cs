﻿namespace ModularImporter.Models.Modular.Data
{
    public class CategoryData
    {
        public int CategoryCode { get; set; }
        public string CategoryName { get; set; } 
    }
}