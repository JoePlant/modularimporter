﻿namespace ModularImporter.Models.Modular.Data
{
    public class ReasonData
    {
        public int CategoryCode { get; set; }
        public int StatusCode { get; set; }
        public int ReasonCode { get; set; }
        public string ReasonName { get; set; }  
    }
}