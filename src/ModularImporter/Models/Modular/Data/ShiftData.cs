﻿using System;

namespace ModularImporter.Models.Modular.Data
{
    public class ShiftData 
    {
        public int ShiftIndex { get; set; }
        public DateTime ShiftStart { get; set;  }
        public string Shift { get; set; }
        public string Crew { get; set; }
    }
}