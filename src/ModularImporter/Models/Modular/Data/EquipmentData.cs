﻿namespace ModularImporter.Models.Modular.Data
{
    public class EquipmentData
    {
        public string EqmtId { get; set; }

        public string EquipmentGroup { get; set; }

        public string EquipmentModel { get; set; }

        public double Size { get; set; }
    }
}