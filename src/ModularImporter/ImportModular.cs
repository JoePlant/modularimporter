﻿using System;
using System.Collections.Generic;
using AmplaData.AmplaData2008;
using ModularImporter.Csv;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Workflow;

namespace ModularImporter
{
    public class ImportModular
    {
        private readonly ICredentialsProvider amplaCredentials;
        private readonly string modularDirectory;

        public ImportModular(ICredentialsProvider amplaCredentials, string modularDirectory)
        {
            this.amplaCredentials = amplaCredentials;
            this.modularDirectory = modularDirectory;
            MaxRecords = int.MaxValue;
            UpdateExistingRecords = false;
        }

        public int MaxRecords { get; set; }

        public bool UpdateExistingRecords { get; set; }

        public void Import()
        {
            CsvModularSettings modularSettings = new CsvModularSettings(modularDirectory);
            CsvModularReader reader = new CsvModularReader(modularSettings);

            List<ExpRoot> expRootRecords = reader.ReadExpRootRecords();
            List<StatusEvent> statusEvents = reader.ReadStatusEventsRecords();
            List<EqmtList> equipListRecords = reader.ReadEqmtListRecords();
            List<OperList> operListRecords = reader.ReadOperListRecords();
            List<Enums> enumsRecords = reader.ReadEnumRecords();
            List<ReasonTable> reasonRecords = reader.ReadReasonTableRecords();
            List<Dumps> dumpRecords = reader.ReadDumpRecords();
            List<Loads> loadRecords = reader.ReadLoadRecords();
            List<GradeList> gradeRecords = reader.ReadGradeRecords();

            IDataResolver dataResolver = new DataResolver(expRootRecords, equipListRecords, operListRecords,
                                                          enumsRecords, reasonRecords);

            DumpsToCycleModelWorkflow cycleWorkflow = new DumpsToCycleModelWorkflow(dataResolver);

            StatusEventToDowntimeWorkflow downtimeWorkflow = new StatusEventToDowntimeWorkflow(dataResolver);
            DumpsToDumpsModelWorkflow dumpsWorkflow = new DumpsToDumpsModelWorkflow(dataResolver);
            LoadsToLoadModelWorkflow loadWorkflow = new LoadsToLoadModelWorkflow(dataResolver);
            GradeListToGradeWorkflow gradeWorkflow = new GradeListToGradeWorkflow(dataResolver);

            using (var client = new DataWebServiceClient("NetTcpBinding_IDataWebService"))
            {
                IAmplaFactory amplaFactory = new AmplaFactory(client, amplaCredentials);

                using (ImportRecords<Dumps, CycleModel, CycleQueryModel> importRecords =
                    new ImportRecords<Dumps, CycleModel, CycleQueryModel>(cycleWorkflow, amplaFactory)
                    {
                        UpdateExistingRecords = UpdateExistingRecords
                    })
                {

                    foreach (Dumps dump in dumpRecords)
                    {
                        importRecords.Import(dump);

                        if (importRecords.ImportedCount > MaxRecords)
                        {
                            break;
                        }
                    }
                    Console.WriteLine("Imported: {0} cycle records", importRecords.ImportedCount);
                }

                /*
                using (ImportRecords<StatusEvent, DowntimeModel, DowntimeQueryModel> importDowntime =
                    new ImportRecords<StatusEvent, DowntimeModel, DowntimeQueryModel>(downtimeWorkflow, amplaFactory)
                        {
                            UpdateExistingRecords = UpdateExistingRecords
                        })
                {

                    foreach (StatusEvent statusEvent in statusEvents)
                    {
                        importDowntime.Import(statusEvent);

                        if (importDowntime.ImportedCount > MaxRecords)
                        {
                            break;
                        }
                    }
                    Console.WriteLine("Imported: {0} downtime records", importDowntime.ImportedCount);
                }
                 */

                /*
                using (ImportRecords<Dumps, DumpModel, DumpQueryModel> importDumpRecords =
                    new ImportRecords<Dumps, DumpModel, DumpQueryModel>(dumpsWorkflow, amplaFactory)
                        {
                            UpdateExistingRecords = UpdateExistingRecords
                        })
                {

                    foreach (Dumps dump in dumpRecords)
                    {
                        importDumpRecords.Import(dump);

                        if (importDumpRecords.ImportedCount > MaxRecords)
                        {
                            break;
                        }
                    }
                    Console.WriteLine("Imported: {0} dump records", importDumpRecords.ImportedCount);
                }
                 */

                /*
                using (ImportRecords<Loads, LoadModel, LoadQueryModel> importRecords =
                    new ImportRecords<Loads, LoadModel, LoadQueryModel>(loadWorkflow, amplaFactory)
                        {
                            UpdateExistingRecords = UpdateExistingRecords
                        })
                {

                    foreach (Loads loads in loadRecords)
                    {
                        importRecords.Import(loads);

                        if (importRecords.ImportedCount > MaxRecords)
                        {
                            break;
                        }
                    }
                    Console.WriteLine("Imported: {0} load records", importRecords.ImportedCount);
                }

                */
                /*
                using (ImportRecords<GradeList, GradeModel, GradeQueryModel> importRecords =
                    new ImportRecords<GradeList, GradeModel, GradeQueryModel>(gradeWorkflow, amplaFactory)
                        {
                            UpdateExistingRecords = UpdateExistingRecords
                        })
                {

                    foreach (GradeList grade in gradeRecords)
                    {
                        importRecords.Import(grade);

                        if (importRecords.ImportedCount > MaxRecords)
                        {
                            break;
                        }
                    }
                    Console.WriteLine("Imported: {0} quality records", importRecords.ImportedCount);
                }
                 */
            }
        }
    }
}