﻿using System.IO;

namespace ModularImporter.Csv
{
    public class CsvModularSettings
    {
        private readonly string directory;

        public CsvModularSettings(string directory)
        {
            this.directory = directory;
            StatusEventsFilename = "statusevents.csv";
            ExpRootFilename = "exproot.csv";
            EqmtListFilename = "eqmtlist.csv";
            OperListFilename = "operlist.csv";
            ReasonTableFilename = "reasontable.csv";
            EnumFilename = "enum.csv";
            DumpsFilename = "dumps.csv";
            GradeListFilename = "gradelist.csv";
            LoadsFilename = "loads.csv";
        }
        
        public CsvModularSettings() : this(".")
        {
        }

        public string ExpRootFilename { get; set; }
        public string EqmtListFilename { get; set; }
        public string OperListFilename { get; set; }
        public string StatusEventsFilename { get; set; }
        public string ReasonTableFilename { get; set; }
        public string EnumFilename { get; set; }
        public string DumpsFilename { get; set; }
        public string GradeListFilename { get; set; }
        public string LoadsFilename { get; set; }

        public string StatusEventsFullPath
        {
            get
            {
                return Path.Combine(directory, StatusEventsFilename);
            }
        }

        public string ExpRootFullPath
        {
            get
            {
                return Path.Combine(directory, ExpRootFilename);
            }
        }

        public string EqmtListFullPath
        {
            get
            {
                return Path.Combine(directory, EqmtListFilename);
            }
        }

        public string OperListFullPath
        {
            get
            {
                return Path.Combine(directory, OperListFilename);
            }
        }

        public string ReasonTableFullPath
        {
            get
            {
                return Path.Combine(directory, ReasonTableFilename);
            }
        }

        public string EnumFullPath
        {
            get
            {
                return Path.Combine(directory, EnumFilename);
            }
        }

        public string DumpsFullPath
        {
            get
            {
                return Path.Combine(directory, DumpsFilename);
            }
        }

        public string GradeListFullPath
        {
            get
            {
                return Path.Combine(directory, GradeListFilename);
            }
        }


        public string LoadsFullPath
        {
            get
            {
                return Path.Combine(directory, LoadsFilename);
            }
        }
    }
}