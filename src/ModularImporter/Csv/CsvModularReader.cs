﻿using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using ModularImporter.ClassMaps;
using ModularImporter.Models.Modular;

namespace ModularImporter.Csv
{
    public class CsvModularReader : IModularReader
    {
        private readonly CsvModularSettings settings;
        private readonly CsvConfiguration configuration;

        public CsvModularReader(CsvModularSettings settings)
        {
            this.settings = settings;
            configuration = new CsvConfiguration();
            configuration.RegisterClassMap<StatusEventsClassMap>();
            configuration.RegisterClassMap<ExpRootClassMap>();
            configuration.RegisterClassMap<EqmtListClassMap>();
            configuration.RegisterClassMap<OperListClassMap>();
            configuration.RegisterClassMap<ReasonTableClassMap>();
            configuration.RegisterClassMap<DumpsClassMap>();
            configuration.RegisterClassMap<GradeListClassMap>();
            configuration.RegisterClassMap<LoadsClassMap>();
            configuration.HasHeaderRecord = false;
            configuration.TrimFields = true;
        }

        protected List<T> ReadRecords<T>(string fileName)
        {
            List<T> list = new List<T>();
            using (TextReader reader = CreateReader(fileName))
            {
                using (CsvReader csv = new CsvReader(reader, configuration))
                {
                    while (csv.Read())
                    {
                        try
                        {
                            T model = csv.GetRecord<T>();
                            list.Add(model);
                        }
                        catch (Exception ex)
                        {
                            string message = string.Format("Error reading row {0}\r\nLine: {1}\r\nFile: {2}", csv.Row,
                                                           string.Join(",", csv.CurrentRecord), fileName);
                            throw new ApplicationException(message, ex);
                        }
                    }
                }
            }
            return list;
        }

        protected virtual TextReader CreateReader(string fileName)
        {
            return new StreamReader(fileName);
        }

        public List<StatusEvent> ReadStatusEventsRecords()
        {
            return ReadRecords<StatusEvent>(settings.StatusEventsFullPath);
        }

        public List<ExpRoot> ReadExpRootRecords()
        {
            return ReadRecords<ExpRoot>(settings.ExpRootFullPath);
        }

        public List<EqmtList> ReadEqmtListRecords()
        {
            return ReadRecords<EqmtList>(settings.EqmtListFullPath);
        }

        public List<OperList> ReadOperListRecords()
        {
            return ReadRecords<OperList>(settings.OperListFullPath);
        }

        public List<ReasonTable> ReadReasonTableRecords()
        {
            return ReadRecords<ReasonTable>(settings.ReasonTableFullPath);
        }

        public List<Enums> ReadEnumRecords()
        {
            return ReadRecords<Enums>(settings.EnumFullPath);
        }

        public List<Dumps> ReadDumpRecords()
        {
            return ReadRecords<Dumps>(settings.DumpsFullPath);
        }

        public List<GradeList> ReadGradeRecords()
        {
            return ReadRecords<GradeList>(settings.GradeListFullPath);
        }

        public List<Loads> ReadLoadRecords()
        {
            return ReadRecords<Loads>(settings.LoadsFullPath);
        }
    }
}