﻿using System;
using System.Collections.Generic;
using AmplaData;
using ModularImporter.Models.Ampla;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class ExistingRecordsUnitTests : TestFixture
    {
        [Test]
        public void ExistingRecords()
        {
            MockAmplaFactory factory = new MockAmplaFactory();
            List<MockQueryModel> records = new List<MockQueryModel> { new MockQueryModel(123), new MockQueryModel(456) };
            MockAmplaRepository<MockQueryModel> repository = new MockAmplaRepository<MockQueryModel>(records);
            factory.Register(repository);

            ExistingRecords<MockQueryModel> existingRecords = new ExistingRecords<MockQueryModel>(factory);
            Assert.That(existingRecords.Count, Is.EqualTo(0));

            existingRecords.Execute();

            Assert.That(existingRecords.Count, Is.EqualTo(2));

            MockQueryModel find = new MockQueryModel(123);

            int recordId;
            Assert.That(existingRecords.TryGetRecordId(find, out recordId), Is.True);
            Assert.That(recordId, Is.EqualTo(1230));
        }

        [Test]
        public void MissingRecord()
        {
            MockAmplaFactory factory = new MockAmplaFactory();
            List<MockQueryModel> records = new List<MockQueryModel> { new MockQueryModel(123), new MockQueryModel(456) };
            MockAmplaRepository<MockQueryModel> repository = new MockAmplaRepository<MockQueryModel>(records);
            factory.Register(repository);

            ExistingRecords<MockQueryModel> existingRecords = new ExistingRecords<MockQueryModel>(factory);
            Assert.That(existingRecords.Count, Is.EqualTo(0));

            existingRecords.Execute();

            Assert.That(existingRecords.Count, Is.EqualTo(2));

            MockQueryModel find = new MockQueryModel(789);

            int recordId;
            Assert.That(existingRecords.TryGetRecordId(find, out recordId), Is.False);
            Assert.That(recordId, Is.EqualTo(0));
        }

        [Test]
        public void FoundAfterAdd()
        {
            MockAmplaFactory factory = new MockAmplaFactory();
            List<MockQueryModel> records = new List<MockQueryModel> { new MockQueryModel(123), new MockQueryModel(456) };
            MockAmplaRepository<MockQueryModel> repository = new MockAmplaRepository<MockQueryModel>(records);
            factory.Register(repository);

            ExistingRecords<MockQueryModel> existingRecords = new ExistingRecords<MockQueryModel>(factory);
            Assert.That(existingRecords.Count, Is.EqualTo(0));

            existingRecords.Execute();

            Assert.That(existingRecords.Count, Is.EqualTo(2));

            MockQueryModel find = new MockQueryModel(789);

            int recordId;
            Assert.That(existingRecords.TryGetRecordId(find, out recordId), Is.False);
            Assert.That(recordId, Is.EqualTo(0));

            existingRecords.Add(new MockQueryModel(789));

            Assert.That(existingRecords.Count, Is.EqualTo(3));

            find = new MockQueryModel(789);

            Assert.That(existingRecords.TryGetRecordId(find, out recordId), Is.True);
            Assert.That(recordId, Is.EqualTo(7890));
        }
    }
}