﻿using System.Collections.Generic;
using ModularImporter.Models.Modular.Data;

namespace ModularImporter.Workflow
{
    public class MockDataResolver : IDataResolver
    {
        private readonly List<ShiftData> shiftData;
        private readonly List<EquipmentData> equipmentData;
        private readonly List<OperatorData> operatorData;
        private readonly List<StatusData> statusData;
        private readonly List<CategoryData> categoryData;
        private readonly List<ReasonData> reasonData;
        private readonly List<LoadData> loadData;

        public MockDataResolver(
            ShiftData shiftData,
            EquipmentData equipmentData,
            OperatorData operatorData,
            StatusData statusData,
            CategoryData categoryData,
            ReasonData reasonData,
            LoadData loadData)
            : this(new List<ShiftData> {shiftData},
                   new List<EquipmentData> {equipmentData},
                   new List<OperatorData> {operatorData},
                   new List<StatusData> {statusData},
                   new List<CategoryData> {categoryData},
                   new List<ReasonData> {reasonData},
                   new List<LoadData> {loadData})
        {
        }

        public MockDataResolver(
            List<ShiftData> shiftData, 
            List<EquipmentData> equipmentData, 
            List<OperatorData> operatorData, 
            List<StatusData> statusData, 
            List<CategoryData> categoryData, 
            List<ReasonData> reasonData,
            List<LoadData> loadData)
        {
            this.shiftData = shiftData;
            this.equipmentData = equipmentData;
            this.operatorData = operatorData;
            this.statusData = statusData;
            this.categoryData = categoryData;
            this.reasonData = reasonData;
            this.loadData = loadData;
        }

        public ShiftData LookupShiftData(int shiftIndex)
        {
            return shiftData.Find(d => d.ShiftIndex == shiftIndex);
        }

        public EquipmentData LookupEquipmentData(string equipmentId)
        {
            return equipmentData.Find(d => d.EqmtId == equipmentId);
        }

        public OperatorData LookupOperatorData(int shiftIndex, string operatorId)
        {
            return operatorData.Find(d => d.ShiftIndex == shiftIndex && d.OperatorId == operatorId);
        }

        public OperatorData LookupOperatorData(string operatorId)
        {
            return operatorData.Find(d => d.OperatorId == operatorId);
        }

        public StatusData LookupStatusData(int statusCode)
        {
            return statusData.Find(d => d.StatusCode == statusCode);
        }

        public CategoryData LookupCategoryData(int categoryCode)
        {
            return categoryData.Find(d => d.CategoryCode == categoryCode);
        }

        public ReasonData LookupReasonData(int categoryCode, int statusCode, int reasonCode)
        {
            return reasonData.Find(
                d => d.CategoryCode == categoryCode
                     && d.StatusCode == statusCode
                     && d.ReasonCode == reasonCode);
        }

        public LoadData LookupLoadData(int loadCode)
        {
            return loadData.Find(d => d.LoadCode == loadCode);
        }
    }
}