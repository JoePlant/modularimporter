﻿using System.Collections.Generic;
using ModularImporter.Csv;
using ModularImporter.Models.Modular;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class DataResolverUnitTests : TestFixture
    {
        [Test]
        public void LookupNoValues()
        {
            DataResolver dataResolver = new DataResolver(new List<ExpRoot>(), new List<EqmtList>(), new List<OperList>(), new List<Enums>(), new List<ReasonTable>());
            Assert.That(dataResolver.LookupShiftData(123), Is.Null);
            Assert.That(dataResolver.LookupEquipmentData("TR01"), Is.Null);
            Assert.That(dataResolver.LookupOperatorData(123, "456"), Is.Null);
            Assert.That(dataResolver.LookupStatusData(4), Is.Null);
            Assert.That(dataResolver.LookupCategoryData(2), Is.Null);
        }

        [Test]
        public void ShiftLookupWithValue()
        {
            CsvModularSettings settings = new CsvModularSettings(@".\Resources\ModularExports");
            CsvModularReader reader = new CsvModularReader(settings);
            List<ExpRoot> expRootRecords = reader.ReadExpRootRecords();
            List<EqmtList> eqmtLists = reader.ReadEqmtListRecords();
            List<OperList> operLists = reader.ReadOperListRecords();
            List<Enums> enumLists = reader.ReadEnumRecords();
            List<ReasonTable> reasonList = reader.ReadReasonTableRecords();
            IDataResolver dataResolver = new DataResolver(expRootRecords, eqmtLists, operLists, enumLists, reasonList);

            Assert.That(dataResolver.LookupShiftData(123), Is.Null);
            Assert.That(dataResolver.LookupShiftData(47123), Is.Not.Null);

            Assert.That(dataResolver.LookupEquipmentData("Tonka"), Is.Null);
            Assert.That(dataResolver.LookupEquipmentData("HD51"), Is.Not.Null);

            Assert.That(dataResolver.LookupOperatorData(000, "000"), Is.Null);
            Assert.That(dataResolver.LookupOperatorData(46291, "000"), Is.Null);
            Assert.That(dataResolver.LookupOperatorData(46291, "029499"), Is.Not.Null);
            Assert.That(dataResolver.LookupOperatorData(000, "029499"), Is.Null);

            Assert.That(dataResolver.LookupOperatorData("000"), Is.Null);
            Assert.That(dataResolver.LookupOperatorData("029499"), Is.Not.Null);

            Assert.That(dataResolver.LookupStatusData(-1), Is.Null);
            Assert.That(dataResolver.LookupStatusData(4), Is.Not.Null);

            Assert.That(dataResolver.LookupCategoryData(-1), Is.Null);
            Assert.That(dataResolver.LookupCategoryData(3), Is.Not.Null);

            Assert.That(dataResolver.LookupLoadData(-1), Is.Null);
            Assert.That(dataResolver.LookupLoadData(3), Is.Not.Null);
        }
    }
}