﻿using System;
using System.Collections.Generic;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class GradeListToGradeWorkflowUnitTests : TestFixture
    {
        private GradeListToGradeWorkflow workflow;

        protected override void OnSetUp()
        {
            base.OnSetUp();
            List<ShiftData> shiftData = new List<ShiftData>
                {
                    new ShiftData
                        {
                            ShiftIndex = 123,
                            Crew = "A",
                            ShiftStart = new DateTime(2013, 2, 1),
                            Shift = "Day Shift"
                        }
                };
            List<EquipmentData> equipmentData = new List<EquipmentData>
                {
                    new EquipmentData
                        {
                            EqmtId = "TR01",
                            EquipmentGroup = "Truck",
                            EquipmentModel = "Tonka Truck",
                            Size = 12.5
                        },
               new EquipmentData
                        {
                            EqmtId = "EV01",
                            EquipmentGroup = "Excavator",
                            EquipmentModel = "Big Digger",
                            Size = 30
                        }
                };
            List<OperatorData> operatorData = new List<OperatorData>
                {
                    new OperatorData
                        {
                            ShiftIndex = 123,
                            OperatorId = "345",
                            OperatorName = "Operator 345",
                            Crew = "G"
                        },
                    new OperatorData
                        {
                            ShiftIndex = 123,
                            OperatorId = "678",
                            OperatorName = "Operator 678",
                            Crew = "G"
                        }
                };
            List<StatusData> statusData = new List<StatusData>
                {
                    new StatusData
                        {
                            StatusCode = 4,
                            StatusName = "Delay"
                        }
                };
            List<CategoryData> categoryData = new List<CategoryData>
                {
                    new CategoryData
                        {
                            CategoryCode = 3,
                            CategoryName = "Breakdown"
                        }
                };
            List<ReasonData> reasonData = new List<ReasonData>
                {
                    new ReasonData
                        {
                            CategoryCode = 3,
                            StatusCode = 4,
                            ReasonCode = 99,
                            ReasonName = "ENGINE PROBLEM"
                        }
                };
            List<LoadData> loadData = new List<LoadData>
                {
                    new LoadData
                        {
                            LoadCode = 3,
                            LoadName = "Topsoil"
                        }
                };
            workflow = new GradeListToGradeWorkflow(new MockDataResolver(shiftData, equipmentData, operatorData, statusData, categoryData, reasonData, loadData));
        }

        protected override void OnTearDown()
        {
            base.OnTearDown();
            workflow = null;
        }

        [Test]
        public void MigrateEmpty()
        {
            GradeList gradeList = new GradeList();
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateInvalidShiftIndex()
        {
            GradeList gradeList = new GradeList { ShiftIndex = 111};
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateSamplePeriod()
        {
            GradeList gradeList = new GradeList { ShiftIndex = 123, LoadNo = 3 };
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.SamplePeriod, Is.EqualTo(new DateTime(2013, 2, 1)));
            Assert.That(model.Crew, Is.EqualTo("A"));
            Assert.That(model.Shift, Is.EqualTo("Day Shift"));
        }

        [Test]
        public void MigrateGrade()
        {
            GradeList gradeList = new GradeList { ShiftIndex = 123, GradeId = "00/00L", LoadNo = 3 };
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Grade, Is.EqualTo("00/00L"));
        }

        [Test]
        public void MigrateLoad()
        {
            GradeList gradeList = new GradeList { ShiftIndex = 123, LoadNo = 3 };
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Material, Is.EqualTo("Topsoil"));
        }

        [Test]
        public void MigrateInvalidLoad()
        {
            GradeList gradeList = new GradeList { ShiftIndex = 123, LoadNo = 255 };
            GradeModel model = workflow.Migrate(gradeList);
            Assert.That(model, Is.Null);
        }
 
    }
}