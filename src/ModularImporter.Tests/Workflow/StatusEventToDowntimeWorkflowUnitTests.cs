﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class StatusEventToDowntimeWorkflowUnitTests : TestFixture
    {
        private StatusEventToDowntimeWorkflow workflow;
        
        protected override void OnSetUp()
        {
            base.OnSetUp();
            ShiftData shiftData = new ShiftData
                        {
                            ShiftIndex = 123,
                            Crew = "A",
                            ShiftStart = new DateTime(2013, 2, 1),
                            Shift = "Day Shift"
                        };
            EquipmentData equipmentData = new EquipmentData
                {
                    EqmtId = "TR01",
                    EquipmentGroup = "Truck",
                    EquipmentModel = "Tonka Truck",
                    Size = 12.5
                };
            OperatorData operatorData = new OperatorData
                {
                    ShiftIndex = 123,
                    OperatorId = "345",
                    OperatorName = "Operator 345",
                    Crew = "G"
                };
            StatusData statusData = new StatusData
                {
                    StatusCode = 4,
                    StatusName = "Delay"
                };
            CategoryData categoryData = new CategoryData
                {
                    CategoryCode = 3,
                    CategoryName = "Breakdown"
                };
            ReasonData reasonData = new ReasonData
                {
                    CategoryCode = 3,
                    StatusCode = 4,
                    ReasonCode = 99,
                    ReasonName = "ENGINE PROBLEM"
                };
            LoadData loadData = new LoadData
                {
                    LoadCode = 3,
                    LoadName = "Topsoil"
                };

            workflow = new StatusEventToDowntimeWorkflow(new MockDataResolver(shiftData, equipmentData, operatorData, statusData, categoryData, reasonData, loadData));
        }

        protected override void OnTearDown()
        {
            base.OnTearDown();
            workflow = null;
        }

        [Test]
        public void MigrateEmpty()
        {
            StatusEvent statusEvent = new StatusEvent();
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateInvalidShiftIndex()
        {
            const int seconds = 3600;
            StatusEvent statusEvent = new StatusEvent { ShiftIndex = 111, StartTime = seconds };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateStartTime()
        {
            const int seconds = 3600;
            StatusEvent statusEvent = new StatusEvent { ShiftIndex = 123, StartTime = seconds, Eqmt = "TR01" };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.StartTime, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds)));
        }

        [Test]
        public void MigrateEquipment()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
                {
                    ShiftIndex = 123,
                    StartTime = seconds,
                    EndTime = seconds*2,
                    Eqmt = "TR01"
                };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Equipment, Is.EqualTo("TR01"));
            Assert.That(model.EquipmentGroup, Is.EqualTo("Truck"));
            Assert.That(model.EquipmentModel, Is.EqualTo("Tonka Truck"));
        }

        [Test]
        public void MigrateEndTime()
        {
            const int seconds = 3600;
            StatusEvent statusEvent = new StatusEvent { ShiftIndex = 123, EndTime = seconds, Eqmt = "TR01"};
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.EndTime, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds)));
        }

        [Test]
        public void MigrateOperator()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345"
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Operator, Is.EqualTo("Operator 345"));
            Assert.That(model.Crew, Is.EqualTo("G"));
        }

        [Test]
        public void MigrateInvalidOperator()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "000"
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Operator, Is.EqualTo("000"));
            Assert.That(model.Crew, Is.EqualTo("A"));
        }

        [Test]
        public void MigrateStatus()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Status = 4,
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularStatus, Is.EqualTo("Delay"));
        }

        [Test]
        public void MigrateInvalidStatus()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Status = 10,
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularStatus, Is.EqualTo("10"));
        }

        [Test]
        public void MigrateCategory()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Category = 3,
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularCategory, Is.EqualTo("Breakdown"));
        }

        [Test]
        public void MigrateInvalidCategory()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Category = 10,
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularCategory, Is.EqualTo("10"));
        }

        [Test]
        public void MigrateReason()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Status = 4,
                Category = 3,
                Reason = 99
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularCategory, Is.EqualTo("Breakdown"));
            Assert.That(model.ModularStatus, Is.EqualTo("Delay"));
            Assert.That(model.ModularReason, Is.EqualTo("ENGINE PROBLEM"));
        }

        [Test]
        public void MigrateInvalidReason()
        {
            const int seconds = 3600;

            StatusEvent statusEvent = new StatusEvent
            {
                ShiftIndex = 123,
                StartTime = seconds,
                EndTime = seconds * 2,
                Eqmt = "TR01",
                OperId = "345",
                Status = 4,
                Category = 3,
                Reason = -1
            };
            DowntimeModel model = workflow.Migrate(statusEvent);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ModularCategory, Is.EqualTo("Breakdown"));
            Assert.That(model.ModularStatus, Is.EqualTo("Delay"));
            Assert.That(model.ModularReason, Is.EqualTo("-1"));
        }

    }
}