﻿using System;
using System.Collections.Generic;
using ModularImporter.Models;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class ImportRecordsUnitTests : TestFixture
    {
        [Test]
        public void ImportADuplicateModel()
        {
            List<MockQueryModel> queryRecords = new List<MockQueryModel>();
            List<MockDataModel> dataRecords = new List<MockDataModel>();

            MockAmplaRepository<MockQueryModel> repositoryQuery = new MockAmplaRepository<MockQueryModel>(queryRecords);
            MockAmplaRepository<MockDataModel> repositoryData = new MockAmplaRepository<MockDataModel>(dataRecords);

            MockAmplaFactory amplaFactory = new MockAmplaFactory();
            amplaFactory.Register(repositoryQuery);
            amplaFactory.Register(repositoryData);

            MockMigrateWorkflow workflow = new MockMigrateWorkflow();
            
            ImportRecords<MockModularModel, MockDataModel, MockQueryModel> importRecords = 
                new ImportRecords<MockModularModel, MockDataModel, MockQueryModel>(workflow, amplaFactory)
                {
                    UpdateExistingRecords = false
                };

            Assert.That(importRecords.ImportedCount, Is.EqualTo(0));
            Assert.That(importRecords.UpdatedCount, Is.EqualTo(0));

            MockModularModel model = new MockModularModel {DdbKey = 123, Truck = "TR01", Tonnes = 100};
            importRecords.Import(model);

            Assert.That(dataRecords.Count, Is.EqualTo(1), "Data Records");

            MockModularModel duplicate = new MockModularModel { DdbKey = 123, Truck = "TR02", Tonnes = 101 };
            importRecords.Import(duplicate);

            Assert.That(dataRecords.Count, Is.EqualTo(1), "Data Records");

            Assert.That(importRecords.ImportedCount, Is.EqualTo(1));
            Assert.That(importRecords.UpdatedCount, Is.EqualTo(0));
        }
        
        [Test]
        public void ImportWithExistingModel()
        {
            List<MockQueryModel> queryRecords = new List<MockQueryModel> {new MockQueryModel(123)};
            List<MockDataModel> dataRecords = new List<MockDataModel>();

            MockAmplaRepository<MockQueryModel> repositoryQuery = new MockAmplaRepository<MockQueryModel>(queryRecords);
            MockAmplaRepository<MockDataModel> repositoryData = new MockAmplaRepository<MockDataModel>(dataRecords);

            MockAmplaFactory amplaFactory = new MockAmplaFactory();
            amplaFactory.Register(repositoryQuery);
            amplaFactory.Register(repositoryData);

            MockMigrateWorkflow workflow = new MockMigrateWorkflow();

            ImportRecords<MockModularModel, MockDataModel, MockQueryModel> importRecords =
                new ImportRecords<MockModularModel, MockDataModel, MockQueryModel>(workflow, amplaFactory)
                {
                    UpdateExistingRecords = false
                };

            Assert.That(importRecords.ImportedCount, Is.EqualTo(0));
            Assert.That(importRecords.UpdatedCount, Is.EqualTo(0));

            MockModularModel model = new MockModularModel { DdbKey = 123, Truck = "TR01", Tonnes = 100 };
            importRecords.Import(model);

            Assert.That(importRecords.ImportedCount, Is.EqualTo(0));
            Assert.That(importRecords.UpdatedCount, Is.EqualTo(0));
        }
    }
}