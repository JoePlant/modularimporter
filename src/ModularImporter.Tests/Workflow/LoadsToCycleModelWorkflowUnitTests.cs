﻿using System;
using System.Collections.Generic;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;
using ModularImporter.Models.Modular.Data;
using NUnit.Framework;

namespace ModularImporter.Workflow
{
    [TestFixture]
    public class LoadsToCycleModelWorkflowUnitTests : TestFixture
    {
        private LoadsToCycleModelWorkflow workflow;

        protected override void OnSetUp()
        {
            base.OnSetUp();
            List<ShiftData> shiftData = new List<ShiftData>
                {
                    new ShiftData
                        {
                            ShiftIndex = 123,
                            Crew = "A",
                            ShiftStart = new DateTime(2013, 2, 1),
                            Shift = "Day Shift"
                        }
                };
            List<EquipmentData> equipmentData = new List<EquipmentData>
                {
                    new EquipmentData
                        {
                            EqmtId = "TR01",
                            EquipmentGroup = "Truck",
                            EquipmentModel = "Tonka Truck",
                            Size = 12.5
                        },
               new EquipmentData
                        {
                            EqmtId = "EV01",
                            EquipmentGroup = "Excavator",
                            EquipmentModel = "Big Digger",
                            Size = 30
                        }
                };
            List<OperatorData> operatorData = new List<OperatorData>
                {
                    new OperatorData
                        {
                            ShiftIndex = 123,
                            OperatorId = "345",
                            OperatorName = "Operator 345",
                            Crew = "G"
                        },
                    new OperatorData
                        {
                            ShiftIndex = 123,
                            OperatorId = "678",
                            OperatorName = "Operator 678",
                            Crew = "G"
                        }
                };
            List<StatusData> statusData = new List<StatusData>
                {
                    new StatusData
                        {
                            StatusCode = 4,
                            StatusName = "Delay"
                        }
                };
            List<CategoryData> categoryData = new List<CategoryData>
                {
                    new CategoryData
                        {
                            CategoryCode = 3,
                            CategoryName = "Breakdown"
                        }
                };
            List<ReasonData> reasonData = new List<ReasonData>
                {
                    new ReasonData
                        {
                            CategoryCode = 3,
                            StatusCode = 4,
                            ReasonCode = 99,
                            ReasonName = "ENGINE PROBLEM"
                        }
                };
            List<LoadData> loadData = new List<LoadData>
                {
                    new LoadData
                        {
                            LoadCode = 3,
                            LoadName = "Topsoil"
                        }
                };
            workflow = new LoadsToCycleModelWorkflow(new MockDataResolver(shiftData, equipmentData, operatorData, statusData, categoryData, reasonData, loadData));
        }

        protected override void OnTearDown()
        {
            base.OnTearDown();
            workflow = null;
        }

        [Test]
        public void MigrateEmpty()
        {
            Loads loads = new Loads();
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateInvalidShiftIndex()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 111, TimrLoad = seconds + 60, TimeFull = seconds };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Null);
        }

        [Test]
        public void MigrateSamplePeriod()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.SamplePeriod, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 60)));
            Assert.That(model.Duration, Is.EqualTo(TimeSpan.FromSeconds(0)));
        }

        [Test]
        public void MigrateTruck()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Truck = "TR01" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Truck, Is.EqualTo("TR01"));
            Assert.That(model.TruckModel, Is.EqualTo("Tonka Truck"));
        }

        [Test]
        public void MigrateExcavator()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Excav = "EV01" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Excavator, Is.EqualTo("EV01"));
            Assert.That(model.ExcavatorModel, Is.EqualTo("Big Digger"));
        }

        [Test]
        public void MigrateTruckOperator()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Truck = "TR01", Oper = "345" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Truck, Is.EqualTo("TR01"));
            Assert.That(model.TruckOperator, Is.EqualTo("Operator 345"));
        }

        [Test]
        public void MigrateExcavatorOperator()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Excav = "EV01", EOper = "678" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Excavator, Is.EqualTo("EV01"));
            Assert.That(model.ExcavatorOperator, Is.EqualTo("Operator 678"));
        }

        [Test]
        public void MigrateTonnes()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, LoadTons = 100 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Tonnes, Is.EqualTo(100d));
        }

        [Test]
        public void MigrateDestination()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Loc = "stockpile" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Destination, Is.EqualTo("stockpile"));
        }

        [Test]
        public void MigrateSource()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Loc = "T/OB" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Source, Is.EqualTo("T/OB"));
        }

        [Test]
        public void MigrateLoad()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Load = 3 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Material, Is.EqualTo("Topsoil"));
        }

        [Test]
        public void MigrateGrade()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60, Grade = "00/00L" };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Grade, Is.EqualTo("00/00L"));
        }

        [Test]
        public void MigrateCount()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.Count, Is.EqualTo(1));
        }

        [Test]
        public void MigrateDumpEnd()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeFull = seconds + 60 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.DumpEnd, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 60)));
            Assert.That(model.DumpEnd, Is.EqualTo(model.SamplePeriod));
        }

        [Test]
        public void MigrateDumpStart()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeDump = seconds + 20, TimeEmpty = seconds + 60, DumpingTim = 40 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.DumpStart, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 20)));
            Assert.That(model.DumpEnd, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 60)));
            Assert.That(model.DumpDuration, Is.EqualTo(TimeSpan.FromSeconds(60 - 20)));
        }

        [Test]
        public void MigrateDumpDuration()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeDump = seconds + 20, TimeEmpty = seconds + 60, DumpingTim = 40 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.DumpStart, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 20)));
            Assert.That(model.DumpEnd, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 60)));
            Assert.That(model.DumpDuration, Is.EqualTo(TimeSpan.FromSeconds(60 - 20)));
        }

        [Test]
        public void MigrateArriveDump()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeDump = seconds + 20, TimeEmpty = seconds + 60, DumpingTim = 40 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.ArriveDump, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds)));
        }

        [Test]
        public void MigrateDumpQueueDuration()
        {
            const int seconds = 3600;
            Loads loads = new Loads { ShiftIndex = 123, TimeArrive = seconds, TimeDump = seconds + 20, TimeEmpty = seconds + 60, DumpingTim = 40, IdleTime = 15 };
            CycleModel model = workflow.Migrate(loads);
            Assert.That(model, Is.Not.Null);
            Assert.That(model.DumpStart, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 20)));
            Assert.That(model.DumpEnd, Is.EqualTo(new DateTime(2013, 2, 1).AddSeconds(seconds + 60)));
            Assert.That(model.DumpQueueDuration, Is.EqualTo(TimeSpan.FromSeconds(15)));
        }

    }
}