﻿using System;
using ModularImporter.Models.Ampla;
using ModularImporter.Models.Modular;

namespace ModularImporter.Workflow
{
    public class MockMigrateWorkflow : MigrateWorkflow<MockModularModel, MockDataModel>
    {
        protected override bool TryMigrate(MockModularModel @from, MockDataModel to)
        {
            to.Key = Convert.ToInt32(from.DdbKey);
            to.Area = from.Truck;
            to.Value = from.Tonnes;
            return true;
        }
    }
}