﻿using System.Collections.Generic;
using AmplaData;
using AmplaData.Records;
using ModularImporter.Models.Ampla;

namespace ModularImporter
{
    public class MockAmplaRepository<TModel> : IRepository<TModel> where TModel : ModularModel
    {
        private readonly List<TModel> records;

        public MockAmplaRepository(List<TModel> records)
        {
            this.records = records;
        }

        public List<TModel> Records
        {
            get { return records; }
        } 

        public void Dispose()
        {
        }

        public IList<TModel> GetAll()
        {
            return new List<TModel>(records);
        }

        public TModel FindById(int id)
        {
            throw new System.NotImplementedException();
        }

        public AmplaRecord FindRecord(int id)
        {
            throw new System.NotImplementedException();
        }

        public IList<TModel> FindByFilter(params FilterValue[] filters)
        {
            throw new System.NotImplementedException();
        }

        public IList<string> ValidateMapping(TModel example)
        {
            throw new System.NotImplementedException();
        }

        public void Add(TModel model)
        {
            model.Id = records.Count+1;
            records.Add(model);
        }

        public void Delete(TModel model)
        {
            throw new System.NotImplementedException();
        }

        public void Update(TModel model)
        {
            throw new System.NotImplementedException();
        }

        public void Confirm(TModel model)
        {
            throw new System.NotImplementedException();
        }

        public void Unconfirm(TModel model)
        {
            throw new System.NotImplementedException();
        }

        public List<string> GetAllowedValues(string property)
        {
            throw new System.NotImplementedException();
        }

        public AmplaAuditRecord GetHistory(int id)
        {
            throw new System.NotImplementedException();
        }

        public ModelVersions GetVersions(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}