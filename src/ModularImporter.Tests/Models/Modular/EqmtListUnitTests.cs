﻿using ModularImporter.Models.Modular.Data;
using NUnit.Framework;

namespace ModularImporter.Models.Modular
{
    [TestFixture]
    public class EqmtListUnitTests : TestFixture
    {
        [Test]
        public void CreateShiftData()
        {
            EqmtList equip = new EqmtList
            {
                EqmtId = "TR01",
                EqmtType = "Tonka Truck",
                Unit = "Truck",
                Size = 10
            };

            EquipmentData data = equip.CreateEquipmentData();

            Assert.That(data.EqmtId, Is.EqualTo("TR01"));
            Assert.That(data.EquipmentGroup, Is.EqualTo("Truck"));
            Assert.That(data.EquipmentModel, Is.EqualTo("Tonka Truck"));
            Assert.That(data.Size, Is.EqualTo(10));
        }
    }
}