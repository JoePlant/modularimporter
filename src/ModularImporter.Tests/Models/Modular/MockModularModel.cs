﻿namespace ModularImporter.Models.Modular
{
    public class MockModularModel : DbModel
    {
        public double Tonnes { get; set; }
        public string Truck { get; set; }
    }
}