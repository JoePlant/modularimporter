﻿using System;
using ModularImporter.Models.Modular.Data;
using NUnit.Framework;

namespace ModularImporter.Models.Modular
{
    [TestFixture]
    public class ExpRootUnitTests : TestFixture
    {
        [Test]
        public void CreateShiftData()
        {
            ExpRoot expRoot = new ExpRoot
                {
                    ShiftIndex = 123,
                    ShiftDate = DateTime.Today,
                    Start = (int) TimeSpan.FromHours(6).TotalSeconds,
                    Shift = "Day Shift",
                    Crew = "A"
                };

            ShiftData shiftData = expRoot.CreateShiftData();
            Assert.That(shiftData.ShiftIndex, Is.EqualTo(123));
            Assert.That(shiftData.ShiftStart, Is.EqualTo(DateTime.Today.AddHours(6)));
            Assert.That(shiftData.Shift, Is.EqualTo("Day Shift"));
            Assert.That(shiftData.Crew, Is.EqualTo("A"));

        }
    }
}