﻿namespace ModularImporter.Models.Ampla
{
    public class MockDataModel : MockQueryModel
    {
        public string Area { get; set; }

        public double Value { get; set; }
    }
}