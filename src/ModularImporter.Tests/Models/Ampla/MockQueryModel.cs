﻿using System;

namespace ModularImporter.Models.Ampla
{
    public class MockQueryModel : ModularModel
    {

        public MockQueryModel()
        {
        }

        public MockQueryModel(int key)
        {
            Key = key;
            Id = key*10;
        }

        public int Key { get; set; }

        public override string UniqueKey()
        {
            return Convert.ToString(Key);
        }
    }
}