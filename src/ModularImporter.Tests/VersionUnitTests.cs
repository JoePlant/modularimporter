﻿using System;
using System.Reflection;
using System.Diagnostics;
using NUnit.Framework;

namespace ModularImporter
{
    [TestFixture]
    public class VersionUnitTests : TestFixture
    {
        private DateTime brisbane = DateTime.UtcNow.AddHours(10);

        protected void CheckAssembly(Assembly assembly)
        {
            Version expected = new Version(1, 0, brisbane.Year, (brisbane.Month * 100) + brisbane.Day);
            Version assemblyVersion = assembly.GetName().Version;
            string fileVersion = FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion;

            Assert.That(assemblyVersion, Is.EqualTo(expected), "{0} Version", assembly.FullName);
            Assert.That(fileVersion, Is.EqualTo(expected.ToString()), "{0} FileVersion", assembly.FullName);
        }

        [Test]
        public void ModularImporter()
        {
            CheckAssembly(typeof(ImportModular).Assembly);
        }

        [Test]
        public void ModularImporterUnitTests()
        {
            CheckAssembly(typeof(TestFixture).Assembly);
        }

    }
}