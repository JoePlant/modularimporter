﻿using NUnit.Framework;

namespace ModularImporter.Csv
{
    [TestFixture]
    public class ModularSettingsUnitTests : TestFixture
    {
        private CsvModularSettings specifiedSettings;
        private CsvModularSettings defaultSettings;

        protected override void OnSetUp()
        {
            base.OnSetUp();
            defaultSettings = new CsvModularSettings();
            specifiedSettings = new CsvModularSettings(@".\Files");
        }

        [Test]
        public void StatusEvents()
        {
            Assert.That(defaultSettings.StatusEventsFullPath, Is.EqualTo(@".\statusevents.csv"));
            Assert.That(specifiedSettings.StatusEventsFullPath, Is.EqualTo(@".\Files\statusevents.csv"));
        }

        [Test]
        public void ExpRoot()
        {
            Assert.That(defaultSettings.ExpRootFullPath, Is.EqualTo(@".\exproot.csv"));
            Assert.That(specifiedSettings.ExpRootFullPath, Is.EqualTo(@".\Files\exproot.csv"));
        }

        [Test]
        public void ReasonTable()
        {
            Assert.That(defaultSettings.ReasonTableFullPath, Is.EqualTo(@".\reasontable.csv"));
            Assert.That(specifiedSettings.ReasonTableFullPath, Is.EqualTo(@".\Files\reasontable.csv"));
        }

        [Test]
        public void EqmtList()
        {
            Assert.That(defaultSettings.EqmtListFullPath, Is.EqualTo(@".\eqmtlist.csv"));
            Assert.That(specifiedSettings.EqmtListFullPath, Is.EqualTo(@".\Files\eqmtlist.csv"));
        }

        [Test]
        public void OperList()
        {
            Assert.That(defaultSettings.OperListFullPath, Is.EqualTo(@".\operlist.csv"));
            Assert.That(specifiedSettings.OperListFullPath, Is.EqualTo(@".\Files\operlist.csv"));
        }

        [Test]
        public void Enums()
        {
            Assert.That(defaultSettings.EnumFullPath, Is.EqualTo(@".\enum.csv"));
            Assert.That(specifiedSettings.EnumFullPath, Is.EqualTo(@".\Files\enum.csv"));
        }

        [Test]
        public void Dumps()
        {
            Assert.That(defaultSettings.DumpsFullPath, Is.EqualTo(@".\dumps.csv"));
            Assert.That(specifiedSettings.DumpsFullPath, Is.EqualTo(@".\Files\dumps.csv"));
        }

        [Test]
        public void GradeList()
        {
            Assert.That(defaultSettings.GradeListFullPath, Is.EqualTo(@".\gradelist.csv"));
            Assert.That(specifiedSettings.GradeListFullPath, Is.EqualTo(@".\Files\gradelist.csv"));
        }

        [Test]
        public void Loads()
        {
            Assert.That(defaultSettings.LoadsFullPath, Is.EqualTo(@".\loads.csv"));
            Assert.That(specifiedSettings.LoadsFullPath, Is.EqualTo(@".\Files\loads.csv"));
        }
    }
}