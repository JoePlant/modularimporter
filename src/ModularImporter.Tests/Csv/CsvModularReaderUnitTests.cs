﻿using System;
using System.Collections.Generic;
using System.IO;
using ModularImporter.Models.Modular;
using NUnit.Framework;

namespace ModularImporter.Csv
{
    [TestFixture]
    public class CsvModularReaderUnitTests : TestFixture
    {
        private CsvModularReader reader;

        protected override void OnSetUp()
        {
            base.OnSetUp();
            CsvModularSettings settings = new CsvModularSettings(@".\Resources\ModularExports");
            reader = new CsvModularReader(settings);
        }

        protected override void OnTearDown()
        {
            reader = null;
            base.OnTearDown();
        }

        [Test]
        public void ReadStatusEvents()
        {
            List<StatusEvent> records = reader.ReadStatusEventsRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadExpRoot()
        {
            List<ExpRoot> records = reader.ReadExpRootRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadEqmtList()
        {
            List<EqmtList> records = reader.ReadEqmtListRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadOperList()
        {
            List<OperList> records = reader.ReadOperListRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadReasonTable()
        {
            List<ReasonTable> records = reader.ReadReasonTableRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadDumps()
        {
            List<Dumps> records = reader.ReadDumpRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadGradeList()
        {
            List<GradeList> records = reader.ReadGradeRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadLoads()
        {
            List<Loads> records = reader.ReadLoadRecords();
            Assert.That(records, Is.Not.Empty);
        }

        [Test]
        public void ReadSingleExpRootRecord()
        {
            const string contents =
                "2013-01-05 00:00:00,33604,1,47130,05-JAN-13 A Shift             ,13,1,JAN,5,1,A Shift   ,1.357366E+09,21600,1,A   ,28800,28800,0,05-01-13 ,2013,1,1,2";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<ExpRoot> results = testReader.ReadExpRootRecords();
            Assert.That(results, Is.Not.Empty);

            ExpRoot record = results[0];

            Assert.That(record.ShiftDate, Is.EqualTo(new DateTime(2013, 01,05)));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(1));
            Assert.That(record.ShiftIndex, Is.EqualTo(47130));
            Assert.That(record.Name, Is.EqualTo("05-JAN-13 A Shift"));
            Assert.That(record.Year, Is.EqualTo(13));
            Assert.That(record.MonthNo,Is.EqualTo(1));
            Assert.That(record.Month, Is.EqualTo("JAN"));
            Assert.That(record.Day, Is.EqualTo(5));
            Assert.That(record.ShiftId, Is.EqualTo(1));
            Assert.That(record.Shift, Is.EqualTo("A Shift"));
            Assert.That(record.Date, Is.EqualTo(1357366000));
            Assert.That(record.Start, Is.EqualTo(21600));
            Assert.That(record.CrewId, Is.EqualTo(1));
            Assert.That(record.Crew, Is.EqualTo("A"));
            Assert.That(record.Len, Is.EqualTo(28800));
            Assert.That(record.DispTime, Is.EqualTo(28800));
            Assert.That(record.Holiday, Is.EqualTo(0));
            Assert.That(record.DdMmYy, Is.EqualTo("05-01-13"));
            Assert.That(record.FinYear, Is.EqualTo(2013));
            Assert.That(record.FinQuarter, Is.EqualTo(1));
            Assert.That(record.FinMonth, Is.EqualTo(1));
            Assert.That(record.FinWeek, Is.EqualTo(2));
        }

        [Test]
        public void ReadSingleStatusEventRecord()
        {
            const string contents = "47118,33604,212662,HD07        ,1,000425              ,9752,13623,3871,1,2,1,                              ,1,28176";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<StatusEvent> results = testReader.ReadStatusEventsRecords();
            Assert.That(results, Is.Not.Empty);

            StatusEvent record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(212662));
            Assert.That(record.Eqmt, Is.EqualTo("HD07"));
            Assert.That(record.Unit, Is.EqualTo(1));
            Assert.That(record.OperId, Is.EqualTo("000425"));
            Assert.That(record.StartTime, Is.EqualTo(9752));
            Assert.That(record.EndTime, Is.EqualTo(13623));
            Assert.That(record.Duration, Is.EqualTo(3871));
            Assert.That(record.Reason, Is.EqualTo(1));
            Assert.That(record.Status, Is.EqualTo(2));
            Assert.That(record.Category, Is.EqualTo(1));
            Assert.That(record.Comment, Is.EqualTo(""));
            Assert.That(record.VEvent, Is.EqualTo(1));
            Assert.That(record.ReasonLink, Is.EqualTo(28176));
        }

        [Test]
        public void ReadSingleEqmtListRecord()
        {
            const string contents =
                "47118,33604,16436,WT10        ,PANCHPATMALI            ,0,18,Water Truck       ,10,Hindustar R35      ,0,0,0,                    ,                    ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<EqmtList> results = testReader.ReadEqmtListRecords();
            Assert.That(results, Is.Not.Empty);

            EqmtList record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(16436));
            Assert.That(record.EqmtId, Is.EqualTo("WT10"));
            Assert.That(record.Pit, Is.EqualTo("PANCHPATMALI"));
            Assert.That(record.Size, Is.EqualTo(0));
            Assert.That(record.UnitNo, Is.EqualTo(18));
            Assert.That(record.Unit, Is.EqualTo("Water Truck"));
            Assert.That(record.EqmtTypeNo, Is.EqualTo(10));
            Assert.That(record.EqmtType, Is.EqualTo("Hindustar R35"));
            Assert.That(record.ExtraLoad, Is.EqualTo(0));
            Assert.That(record.Trammer, Is.EqualTo(0));
            Assert.That(record.Performance, Is.EqualTo(0));
            Assert.That(record.OperId, Is.EqualTo(""));
            Assert.That(record.OperName, Is.EqualTo(""));
        }

        [Test]
        public void ReadSingleOperListRecord()
        {
            const string contents =
                "46290,33604,21802,29285,A B Surname             ,4,G   ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<OperList> results = testReader.ReadOperListRecords();
            Assert.That(results, Is.Not.Empty);

            OperList record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(46290));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(21802));
            Assert.That(record.OperId, Is.EqualTo("29285"));
            Assert.That(record.Name, Is.EqualTo("A B Surname"));
            Assert.That(record.CrewNo, Is.EqualTo(4));
            Assert.That(record.Crew, Is.EqualTo("G"));
        }

        [Test]
        public void ReadEqmtListWithDecimalInSize()
        {
            const string contents =
                "47118,33604,134100,HE09        ,PANCHPATMALI            ,6.5,2,Shovel            ,26,Tata-Hitachi EX1200,0,0,0,                    ,                    ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<EqmtList> results = testReader.ReadEqmtListRecords();
            Assert.That(results, Is.Not.Empty);

            Assert.That(results[0].Size, Is.EqualTo(6.5));
        }

        [Test]
        public void ReadEqmtListWithDecimalInPerformance()
        {
            const string contents =
                "47182,33604,14528,DR12        ,K/BXT(R/D)              ,0,12,Drill             ,5,IR IDM30           ,0,0,119.5,                    ,                    ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<EqmtList> results = testReader.ReadEqmtListRecords();
            Assert.That(results, Is.Not.Empty);

            Assert.That(results[0].Performance, Is.EqualTo(119.5));
        }

        [Test]
        public void ReadDumpsWithDecimalInTonnes()
        {
            const string contents =
                "47210,33604,32068,WL20        ,STOCKPILE_1             ,117/13M             ,WL20        ,U/BXT                   ,            ,0,0,1934,14774,21156,0,0,4,0,0,0,0,0,0,0,1,8.5,32990,1,029491              ,mmsunk              ,0,0,0,0,1,4,04:00:00 ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<Dumps> results = testReader.ReadDumpRecords();
            Assert.That(results, Is.Not.Empty);

            Assert.That(results[0].DumpTons, Is.EqualTo(8.5));
        }

        [Test]
        public void ReadLoadsWithDecimalInTonnes()
        {
            const string contents =
                "47369,33604,27328,WL20        ,WL20        ,146/50M             ,STOCKPILE_1             ,0,0,1934,2551,19772,0,0,0,0,7,0,0,0,0,0,0,0,0,0,1,8.5,32042,mmsunk              ,mmsunk              ,1934,0,0,16860,0,0,0,0,7341,0,0,2,0,0,1,0,0,1,1,0,1,1,0,0,0,0,28800,12028,4599,0,12173,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,28800,12028,4599,0,12173,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,05:00:00 ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<Loads> results = testReader.ReadLoadRecords();
            Assert.That(results, Is.Not.Empty);

            Assert.That(results[0].LoadTons, Is.EqualTo(8.5));
        }

        [Test]
        public void ReadSingleReasonTableRecord()
        {
            const string contents =
                "47118,33604,26934,BOULDER IN ROM              ,9009,1,1200,3,NO  ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<ReasonTable> results = testReader.ReadReasonTableRecords();
            Assert.That(results, Is.Not.Empty);

            ReasonTable record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(26934));
            Assert.That(record.Name, Is.EqualTo("BOULDER IN ROM"));
            Assert.That(record.Reason, Is.EqualTo(9009));
            Assert.That(record.Status, Is.EqualTo(1));
            Assert.That(record.DelayTime, Is.EqualTo(1200));
            Assert.That(record.Category, Is.EqualTo(3));
            Assert.That(record.MainTime, Is.EqualTo("NO"));
        }

        //
        [Test]
        public void ReasonTableRecordWithNullDdbKey()
        {
            const string contents =
                "47145,33604,NULL,mmsunk                      ,0,0,0,3,no  ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<ReasonTable> results = testReader.ReadReasonTableRecords();
            Assert.That(results, Is.Not.Empty);

            ReasonTable record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47145));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(0));
            Assert.That(record.Name, Is.EqualTo("mmsunk"));
            Assert.That(record.Reason, Is.EqualTo(0));
            Assert.That(record.Status, Is.EqualTo(0));
            Assert.That(record.DelayTime, Is.EqualTo(0));
            Assert.That(record.Category, Is.EqualTo(3));
            Assert.That(record.MainTime, Is.EqualTo("no"));
        }

        [Test]
        public void ReadSingleDumpsRecord()
        {
            const string contents =
                "47118,33604,175878,HD65        ,STOCKPILE_1             ,109/10M             ,HE10        ,V/BXT                   ,            ,93078,0,17264,17264,17345,0,304,1,0,50,7,1968,871,3170,0,0,45,104750,442,000461              ,029116              ,0,0,81,1,0,4,04:00:00 ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<Dumps> results = testReader.ReadDumpRecords();
            Assert.That(results, Is.Not.Empty);

            Dumps record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(175878));
            Assert.That(record.Truck, Is.EqualTo("HD65"));
            Assert.That(record.Loc, Is.EqualTo("STOCKPILE_1"));
            Assert.That(record.Grade, Is.EqualTo("109/10M"));
            Assert.That(record.Excav, Is.EqualTo("HE10"));
            Assert.That(record.Blast, Is.EqualTo("V/BXT"));
            Assert.That(record.Bay, Is.EqualTo(""));
            Assert.That(record.LoadRec, Is.EqualTo(93078));
            Assert.That(record.MeasureTon, Is.EqualTo(0));
            Assert.That(record.TimeArrive, Is.EqualTo(17264));
            Assert.That(record.TimeDump, Is.EqualTo(17264));
            Assert.That(record.TimeEmpty, Is.EqualTo(17345));
            Assert.That(record.TimeDigest, Is.EqualTo(0));
            Assert.That(record.CalcTravTi, Is.EqualTo(304));
            Assert.That(record.Load, Is.EqualTo(1));
            Assert.That(record.ExtraLoad, Is.EqualTo(0));
            Assert.That(record.LiftUp, Is.EqualTo(50));
            Assert.That(record.LiftDown, Is.EqualTo(7));
            Assert.That(record.LiftDistU, Is.EqualTo(1968));
            Assert.That(record.LiftDistD, Is.EqualTo(871));
            Assert.That(record.Dist, Is.EqualTo(3170));
            Assert.That(record.Efh, Is.EqualTo(0));
            Assert.That(record.LoadType, Is.EqualTo(0));
            Assert.That(record.DumpTons, Is.EqualTo(45));
            Assert.That(record.ShiftLink, Is.EqualTo(104750));
            Assert.That(record.DumpId, Is.EqualTo(442));
            Assert.That(record.Oper, Is.EqualTo("000461"));
            Assert.That(record.EOper, Is.EqualTo("029116"));
            Assert.That(record.IdleTime, Is.EqualTo(0));
            Assert.That(record.LoadNumber, Is.EqualTo(0));
            Assert.That(record.DumpingTim, Is.EqualTo(81));
            Assert.That(record.ValDmp, Is.EqualTo(1));
            Assert.That(record.IDmp, Is.EqualTo(0));
            Assert.That(record.Hos, Is.EqualTo(4));
            Assert.That(record.Intvl, Is.EqualTo(TimeSpan.FromHours(4)));
        }

        [Test]
        public void ReadSingleGradeRecord()
        {
            const string contents =
                "47118,33604,23724,115/13M             ,                        ,                        ,1000,42.75,1.93,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2.5,1,Main Grade  ,0,0  ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<GradeList> results = testReader.ReadGradeRecords();
            Assert.That(results, Is.Not.Empty);

            GradeList record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(23724));
            Assert.That(record.GradeId, Is.EqualTo("115/13M"));
            Assert.That(record.Loc, Is.EqualTo(""));
            Assert.That(record.Dump, Is.EqualTo(""));
            Assert.That(record.Inv, Is.EqualTo(1000));
            Assert.That(record.Grade00, Is.EqualTo(42.75));
            Assert.That(record.Grade01, Is.EqualTo(1.93));
            Assert.That(record.Grade02, Is.EqualTo(0));
            Assert.That(record.Grade03, Is.EqualTo(0));
            Assert.That(record.Grade04, Is.EqualTo(0));
            Assert.That(record.Grade05, Is.EqualTo(0));
            Assert.That(record.Grade06, Is.EqualTo(0));
            Assert.That(record.Grade07, Is.EqualTo(0));
            Assert.That(record.Grade08, Is.EqualTo(0));
            Assert.That(record.Grade09, Is.EqualTo(0));
            Assert.That(record.Grade10, Is.EqualTo(0));
            Assert.That(record.Grade11, Is.EqualTo(0));
            Assert.That(record.Grade12, Is.EqualTo(0));
            Assert.That(record.Grade13, Is.EqualTo(0));
            Assert.That(record.Grade14, Is.EqualTo(0));
            Assert.That(record.Grade15, Is.EqualTo(0));
            Assert.That(record.Grade16, Is.EqualTo(0));
            Assert.That(record.Grade17, Is.EqualTo(0));
            Assert.That(record.Grade18, Is.EqualTo(0));
            Assert.That(record.Grade19, Is.EqualTo(0));
            Assert.That(record.Grade20, Is.EqualTo(0));
            Assert.That(record.Grade21, Is.EqualTo(0));
            Assert.That(record.Grade22, Is.EqualTo(0));
            Assert.That(record.Grade23, Is.EqualTo(0));
            Assert.That(record.Grade24, Is.EqualTo(0));
            Assert.That(record.Spgr, Is.EqualTo(2.5));
            Assert.That(record.LoadNo, Is.EqualTo(1));
            Assert.That(record.Load, Is.EqualTo("Main Grade"));
            Assert.That(record.BlendNo, Is.EqualTo(0));
            Assert.That(record.Blend, Is.EqualTo(0));
        }
        
        [Test]
        public void ReadSingleLoadsRecord()
        {
            const string contents =
                "47118,33604,36792,HD06        ,HE09        ,134/59B             ,N6/BBXT                 ,142596,0,4194,4280,4539,505,302,0,726,2,0,38,49,2924,2867,6453,3441,0,0,0,50,45958,000427              ,029067              ,4194,0,86,259,726,8,0,23,0,27,11,20,1,1,0,1,0,0,0,0,1,0,0,0,0,1,5273,1886,3387,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,368,368,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,01:00:00 ";
            TestCsvModularReader testReader = new TestCsvModularReader(contents);
            List<Loads> results = testReader.ReadLoadRecords();
            Assert.That(results, Is.Not.Empty);

            Loads record = results[0];

            Assert.That(record.ShiftIndex, Is.EqualTo(47118));
            Assert.That(record.CliId, Is.EqualTo(33604));
            Assert.That(record.DdbKey, Is.EqualTo(36792));
            Assert.That(record.Truck, Is.EqualTo("HD06"));
            Assert.That(record.Excav, Is.EqualTo("HE09"));
            Assert.That(record.Grade, Is.EqualTo("134/59B"));
            Assert.That(record.Loc, Is.EqualTo("N6/BBXT"));
            Assert.That(record.DumpRec, Is.EqualTo(142596));
            Assert.That(record.MeasureTon, Is.EqualTo(0));
            Assert.That(record.TimeArrive, Is.EqualTo(4194));
            Assert.That(record.TimrLoad, Is.EqualTo(4280));
            Assert.That(record.TimeFull, Is.EqualTo(4539));
            Assert.That(record.Ctrteh, Is.EqualTo(505));
            Assert.That(record.Ctrtfh, Is.EqualTo(302));
            Assert.That(record.Atrteh, Is.EqualTo(0));
            Assert.That(record.Atrtfh, Is.EqualTo(726));
            Assert.That(record.Load, Is.EqualTo(2));
            Assert.That(record.ExtraLoad, Is.EqualTo(0));
            Assert.That(record.Lift_Up, Is.EqualTo(38));
            Assert.That(record.Lift_Down, Is.EqualTo(49));
            Assert.That(record.LiftDist_U, Is.EqualTo(2924));
            Assert.That(record.LiftDist_D, Is.EqualTo(2867));
            Assert.That(record.Disteh, Is.EqualTo(6453));
            Assert.That(record.Distfh, Is.EqualTo(3441));
            Assert.That(record.Efheh, Is.EqualTo(0));
            Assert.That(record.Efhfh, Is.EqualTo(0));
            Assert.That(record.Loadtype, Is.EqualTo(0));
            Assert.That(record.LoadTons, Is.EqualTo(50));
            Assert.That(record.ShiftLink, Is.EqualTo(45958));
            Assert.That(record.Oper, Is.EqualTo("000427"));
            Assert.That(record.EOper, Is.EqualTo("029067"));
            Assert.That(record.BeginSpot, Is.EqualTo(4194));
            Assert.That(record.QueueTime, Is.EqualTo(0));
            Assert.That(record.SpotTime, Is.EqualTo(86));
            Assert.That(record.LoadingTim, Is.EqualTo(259));
            Assert.That(record.FullHaul, Is.EqualTo(726));
            Assert.That(record.DumpTime, Is.EqualTo(8));
            Assert.That(record.EmptyHaul, Is.EqualTo(0));
            Assert.That(record.HangTime, Is.EqualTo(23));
            Assert.That(record.IdleTime, Is.EqualTo(0));
            Assert.That(record.ExcavNextL, Is.EqualTo(27));
            Assert.That(record.ExcavPrevL, Is.EqualTo(11));
            Assert.That(record.LoadNumber, Is.EqualTo(20));
            Assert.That(record.Val_sp, Is.EqualTo(1));
            Assert.That(record.Val_ld, Is.EqualTo(1));
            Assert.That(record.Val_mt, Is.EqualTo(0));
            Assert.That(record.Val_spld, Is.EqualTo(1));
            Assert.That(record.Val_spldmt, Is.EqualTo(0));
            Assert.That(record.Val_ehaul, Is.EqualTo(0));
            Assert.That(record.Val_fhaul, Is.EqualTo(0));
            Assert.That(record.Ot, Is.EqualTo(0));
            Assert.That(record.Ut, Is.EqualTo(1));
            Assert.That(record.Isp, Is.EqualTo(0));
            Assert.That(record.Ild, Is.EqualTo(0));
            Assert.That(record.Ifh, Is.EqualTo(0));
            Assert.That(record.Ieh, Is.EqualTo(0));
            Assert.That(record.Dmp, Is.EqualTo(1));
            Assert.That(record.Tk_TmCat00, Is.EqualTo(5273));
            Assert.That(record.Tk_TmCat01, Is.EqualTo(1886));
            Assert.That(record.Tk_TmCat02, Is.EqualTo(3387));
            Assert.That(record.Tk_TmCat03, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat04, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat05, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat06, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat07, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat08, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat09, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat10, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat11, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat12, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat13, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat14, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat15, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat16, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat17, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat18, Is.EqualTo(0));
            Assert.That(record.Tk_TmCat19, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat00, Is.EqualTo(368));
            Assert.That(record.Ex_TmCat01, Is.EqualTo(368));
            Assert.That(record.Ex_TmCat02, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat03, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat04, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat05, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat06, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat07, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat08, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat09, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat10, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat11, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat12, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat13, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat14, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat15, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat16, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat17, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat18, Is.EqualTo(0));
            Assert.That(record.Ex_TmCat19, Is.EqualTo(0));
            Assert.That(record.Hos, Is.EqualTo(1));
            Assert.That(record.Intvl, Is.EqualTo(TimeSpan.FromHours(1)));
        }

        public class TestCsvModularReader : CsvModularReader
        {
            private readonly string fileContents;

            public TestCsvModularReader(string contents) : base(new CsvModularSettings())
            {
                this.fileContents = contents;
            }

            protected override TextReader CreateReader(string fileName)
            {
                return new StringReader(fileContents);
            }
        }

    }
}