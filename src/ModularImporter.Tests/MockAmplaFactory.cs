﻿using System;
using System.Collections.Generic;
using AmplaData;

namespace ModularImporter
{
    public class MockAmplaFactory : IAmplaFactory
    {
        private readonly Dictionary<Type, object> repositories;

        public MockAmplaFactory()
        {
            repositories = new Dictionary<Type, object>();
        }

        public void Register<TModel>(IRepository<TModel> repository)
        {
            repositories[typeof (TModel)] = repository;
        }

        public IRepository<TModel> GetRepository<TModel>() where TModel : class, new()
        {
            object repository;
            if (repositories.TryGetValue(typeof (TModel), out repository))
            {
                return repository as IRepository<TModel>;
            }
            return null;
        }
    }
}