﻿using System.Collections.Generic;
using AmplaData.AmplaData2008;

namespace ModularImporter.Console
{
    class Program
    {
        private static void Main(string[] args)
        {
            CredentialsProvider credentials = CredentialsProvider.ForUsernameAndPassword("User", "password");
            string directory = args[0];

            ValidateModels validateModels = new ValidateModels(credentials);
            List<string> messages = validateModels.Validate();
            if (messages.Count == 0)
            {
                System.Console.WriteLine("Models validated with no errors.");
            }
            foreach (string message in messages)
            {
                System.Console.WriteLine(message); 
            }

            if (messages.Count == 0)
            {
                System.Console.WriteLine("Directory:{0}", directory);
                ImportModular importModular = new ImportModular(credentials, directory)
                    {
                        MaxRecords = 100,
                        UpdateExistingRecords = false
                    };
                importModular.Import();
            }

        }
    }
}
